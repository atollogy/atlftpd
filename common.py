#!/usr/bin/env python3

import os
import sys
here = os.path.dirname(os.path.realpath('atlftpd'))

from attribute_dict import *
from log import *

import base64
import cv2
from dateutil.parser import parse as date_parser
from datetime import datetime, date, timedelta, timezone
import functools
import gzip
import iso8601
import io
import json
import mimetypes
import numpy as np
import re
import requests
requests.packages.urllib3.util.ssl_.DEFAULT_CIPHERS = 'ALL'
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
import signal
import subprocess as _sp
import time
from typing import Optional, List, Dict, Tuple, Union
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
import zipfile



BCM = None
def get_bcm():
    global BCM
    cfg = None
    local = False
    if "local_bcm" in os.environ:
        cfg = os.environ["local_bcm"]
        local = True
    elif os.path.exists("/etc/kvhandler/bcm.json"):
        cfg = "/etc/kvhandler/bcm.json"
    elif os.path.exists("/etc/kvmanager/bcm.json"):
        cfg = "/etc/kvmanager/bcm.json"

    if cfg is not None:
        BCM = AD.load(cfg)
        BCM.is_local = local
        return BCM
    else:
        print("BCM cannot be found - exiting")
        sys.exit(1)

get_bcm()


class Atl_Error(Exception):
    def __init__(self, message, status_code=400, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv["message"] = self.message
        return rv

def _httpGet(*args, **kwargs):
    if "url" not in kwargs:
        raise Atl_Error("_http_get_json Error: URL is a required parameter")
    elif len(kwargs["url"]) < 10:
        raise Atl_Error("_http_get_json Error: Invalid URL passed: {}".format(kwargs["url"]))
    else:
        if "backoff_factor" not in kwargs:
            kwargs["backoff_factor"] = 0.3
        if "headers" not in kwargs or kwargs["headers"] == None:
            kwargs["headers"] = {}
        if "params" not in kwargs:
            kwargs["params"] = {}
        if "retries" not in kwargs:
            kwargs["retries"] = 3
        if "status_forcelist" not in kwargs:
            kwargs["status_forcelist"] = (500, 502, 504)
        if "timeout" not in kwargs or kwargs["timeout"] == None:
            kwargs["timeout"] = 100
        if "verify" not in kwargs:
            kwargs["verify"] = None
        resp = requests_retry_session(
                    backoff_factor=kwargs["backoff_factor"],
                    retries=kwargs["retries"],
                    status_forcelist=kwargs["status_forcelist"]
                ).get(
                url=kwargs["url"],
                headers=kwargs["headers"],
                params=kwargs["params"],
                timeout=kwargs["timeout"],
                verify=kwargs["verify"]
            )
        resp.raise_for_status()
        return resp

def _httpPost( *args, **kwargs):
    if "payload" not in kwargs:
        raise Atl_Error("_http_post_json Error: Empty payload")
    elif "url" not in kwargs:
        raise Atl_Error("_http_post_json Error: URL is a required parameter")
    elif len(kwargs["url"]) < 10:
        raise Atl_Error("_http_post_json Error: Invalid URL passed: {}".format(kwargs["url"]))
    else:
        if "backoff_factor" not in kwargs:
            kwargs["backoff_factor"] = 0.3
        if "headers" not in kwargs or kwargs["headers"] == None:
            kwargs["headers"] = {}
        if "params" not in kwargs:
            kwargs["params"] = {}
        if "retries" not in kwargs:
            kwargs["retries"] = 3
        if "status_forcelist" not in kwargs:
            kwargs["status_forcelist"] = (500, 502, 504)
        if "timeout" not in kwargs or kwargs["timeout"] == None:
            kwargs["timeout"] = 100
        if "verify" not in kwargs:
            kwargs["verify"] = None
        resp = requests_retry_session(
                    backoff_factor=kwargs["backoff_factor"],
                    retries=kwargs["retries"],
                    status_forcelist=kwargs["status_forcelist"],
                ).post(
                url=kwargs["url"],
                headers=kwargs["headers"],
                params=kwargs["params"],
                json=kwargs["payload"],
                timeout=kwargs["timeout"],
                verify=kwargs["verify"],
            )
        resp.raise_for_status()
        return resp

def requests_retry_session(backoff_factor=1, status_forcelist=(500, 502, 503, 504), retries=3, session=None):
    session = session or requests.Session()
    retry = Retry(
        total=retries,
        read=retries,
        connect=retries,
        backoff_factor=backoff_factor,
        status_forcelist=status_forcelist,
    )
    adapter = HTTPAdapter(max_retries=retry)
    session.mount("http://", adapter)
    session.mount("https://", adapter)
    return session

mdata_endpoint = f"https://mdata.{BCM.env}.at0l.io"
def get_mdata(key):
    try:
        if '.' in key:
            key = key.replace('.', '/')
        if key[0] == '/':
            key = key[1:]
        return get_rest_api(f"{mdata_endpoint}/kvq/tree/{key}")
    except Exception as err:
        logger.info(f"atlc-base.api.get_mdata (key: {key}) requests GET error: {err}")
        return None

def get_rest_api(url, headers=None, params=None, retries=3, timeout=10, verify=None):
    resp = _httpGet(
        url=url,
        headers=headers,
        params=params,
        timeout=timeout,
        verify=verify,
    )
    if resp.status_code < 300:
        return resp
    else:
        logger.error(resp)
        return None

def post_rest_api(url, payload, headers=None, params=None, retries=3, timeout=10, verify=None):
    if not isinstance(payload, str):
        payload = _json_safe(payload)
    resp = _httpPost(
        url=url,
        payload=payload,
        headers=headers,
        params=params,
        timeout=timeout,
        verify=verify,
    )

    if resp.status_code < 300:
        return resp
    else:
        logger.error(resp)
        return resp

### JSON Serialization Helpers
class JSONEncoder(json.JSONEncoder):
    """JSONEncoder to handle ``datetime`` and other problematic object values"""

    class EncodeError(Exception):
        """Raised when an error occurs during encoding"""

    def default(self, obj):
        try:
            if isinstance(obj, (datetime)):
                return obj.timestamp()
            elif isinstance(obj, bytes):
                return self.default(obj.decode("utf-8"))
            elif isinstance(obj, io.BytesIO):
                return "removed binary data"
            elif hasattr(obj, "toJSON"):
                return obj.toJSON()
            elif hasattr(obj, "jstr"):
                return obj.jstr()
            else:
                try:
                    encoded_obj = json.JSONEncoder.default(self, obj)
                except TypeError as err:
                    encoded_obj = repr(obj)
                return encoded_obj
        except Exception as err:
            raise JSONEncoder.EncodeError("JSON Encoder Error: {}".format(repr(err)))

### json object serializer
def json_safe(obj):
    """JSON dumper for objects not serializable by default json code"""
    try:
        jstr = json.dumps(obj, cls=JSONEncoder, default=str, indent=4, separators=(",", ": "), sort_keys=True)
    except Exception:
        jstr = repr(obj)
    return jstr


def load_image(raw_image: Union[bytearray, bytes, str, io.BytesIO, np.ndarray],
               b64: bool=False,
               color_space: str='bgr',
               crop: Union[tuple, list]=None) -> np.ndarray:
    try:
        image = img_nda = None
        color_space = color_space.lower()
        if b64:
            img_nda = np.asarray(bytearray(base64.b64decode(raw_image.encode())), dtype=np.uint8)
        elif isinstance(image, io.BytesIO):
            img_nda = np.asarray(bytearray(raw_image.getvalue()), dtype=np.uint8)
        elif isinstance(raw_image, bytearray):
            img_nda = np.asarray(raw_image, dtype=np.uint8)
        elif isinstance(raw_image, bytes):
            img_nda = np.asarray(bytearray(raw_image), dtype=np.uint8)
        elif isinstance(raw_image, str):
            if os.path.exists(raw_image):
                image = raw_image = cv2.imread(raw_image)
            else:
                img_nda = np.asarray(bytearray(raw_image.encode()), dtype=np.uint8)
        elif isinstance(raw_image, np.ndarray) and raw_image.ndim == 1:
            img_nda = raw_image
        elif isinstance(raw_image, np.ndarray) and raw_image.ndim == 2:
            img_nda = raw_image
        elif isinstance(raw_image, np.ndarray) and raw_image.ndim >= 3:
            image = raw_image.copy()
        else:
             raise Image_Utility_Exception(f"image_utilities.load_image exception un-supported input image format: {type(raw_image)}")
        if image is None:
            image = cv2.imdecode(img_nda, cv2.IMREAD_COLOR)
        if crop and isinstance(crop, (list, tuple)) and len(crop) == 4:  # assumes [x, y, w, h]
            x_slice = slice(crop[0], crop[0] + crop[2])
            y_slice = slice(crop[1], crop[1] + crop[3])
            image = image[y_slice, x_slice]
        if color_space and color_space != 'bgr':
            return color_space_convert(image, 'bgr', color_space)
        else:
            return image
    except Exception as err:
        raise Image_Utility_Exception(f"image_utilities.load_image exception un-supported input image format: " + repr(err))

FQDND_STORE = AD()
host_re = re.compile(r'(?P<scode>(^(\w){4,8})(?=(?P<inst>(\d{2,4}$))))')
def parse_fqdn(hostname, bcm=None):
    if hostname is None or not isinstance(hostname, str) or len(hostname) < 3:
        logger.info(f"parse_fqdn: cannot deal with - {hostname}")
        return None

    hostname = hostname.strip()
    if '.' not in hostname and '_' in hostname:
        hostname = '.'.join((hostname.split('_') + ['at0l.io']))
    elif '.' not in hostname:
        hostname = f"{hostname}.{BCM.cgr}.{BCM.env}.at0l.io"

    if hostname in FQDND_STORE:
        return FQDND_STORE[hostname]
    else:
        FQDND_STORE[hostname] = fqdnd = AD()

    nparts = hostname.split('.')
    fqdnd.fqdn = fqdnd.hostname = hostname
    fqdnd.cgr = fqdnd.customer_id = nparts[-4]
    fqdnd.dom = nparts[-2]
    fqdnd.domain = '.'.join(nparts[-2:])
    fqdnd.env = nparts[-3]
    fqdnd.host = nparts[0]
    fqdnd.nodename = '_'.join(nparts[:-2])
    fqdnd.nparts = nparts
    fqdnd.shost = '.'.join(nparts[-3:])
    fqdnd.site = None
    fqdnd.subdom = '.'.join(nparts[1:])
    fqdnd.tld = nparts[-1]
    fqdnd.scode = fqdnd.host
    fqdnd.inst = 0
    fqdnd.inum = 0
    fqdnd.slot = 0

    labels = ['host', 'site', 'cgr', 'env', 'dom', 'tld']
    splabels = labels[-(len(nparts) - 1):]
    nsubparts = nparts[-(len(nparts) - 1):]
    for idx, np in enumerate(nsubparts):
        fqdnd[splabels[idx]] = np

    try:
        res = host_re.match(fqdnd.host)
        if hasattr(res, 'groupdict'):
            hre_res = res.groupdict()
            fqdnd.scode = hre_res['scode']
            fqdnd.inst = hre_res['inst']
            fqdnd.inum = int(hre_res['inst'])
            fqdnd.slot = int(hre_res['inst'][-1])
        return fqdnd
    except Exception as err:
        logger.info(f'parse_fqdn exception: {err}')
        return fqdnd

def shcmd(cmd, si=None, shell=True):
    """Execute command using subprocess and returns a tuple (cmd output, cmd stderr, result code)"""

    rawcmd = []
    if not isinstance(cmd, list):
        cmd = [cmd]

    for c in cmd:
        rawcmd.append("%s" % (c))

    cmdobj = _sp.Popen(rawcmd, stdin=si, stdout=_sp.PIPE, stderr=_sp.PIPE, shell=shell)

    try:
        (cmdout, cmderr) = cmdobj.communicate()
        cmdout = cmdout.decode().split("\n")
        res = (cmdout, cmderr, cmdobj.returncode)
        return res
    except OSError as e:
        return (None, e, cmdobj.returncode)

######### datetime handler ############################################
# this regex parses '20201111120151' and '20201111120151.123456'
DATETIME_RE = re.compile(r'(?P<year>(\d{4}))(?P<month>(\d{2}))(?P<day>(\d{2}))(?P<hour>(\d{2}))(?P<minute>(\d{2}))(?P<second>(\d{2}))\.{0,1}(?P<microsecond>(\d{0,6}))')
def to_datetime(rec_time):
    now = datetime.utcnow()
    now = now.replace(tzinfo=timezone.utc)
    if not rec_time:
        msg = f"to_datetime ERROR: rec_time is NULL"
        logger.error(msg)
        return rec_time
    elif isinstance(rec_time, str):
        dmatch = DATETIME_RE.match(rec_time)
        if any([c in rec_time for c in ['-', 'T']]):
            dt = iso8601.parse_date(rec_time)
            dt = dt.replace(tzinfo=timezone.utc)
        elif len(rec_time) in [14, 21] and hasattr(dmatch, 'groupdict'):
            dtm = AD({k: int(v) for k, v in dmatch.groupdict().items() if v.isnumeric()})
            if 'microsecond' not in dtm:
                dtm.microsecond = datetime.utcnow().microsecond
            dt = datetime(dtm.year, dtm.month, dtm.day, hour=dtm.hour, minute=dtm.minute, second=dtm.second, microsecond=dtm.microsecond, tzinfo=timezone.utc)
        elif '.' in rec_time:
            try:
                dt = datetime.fromtimestamp(float(rec_time), tz=timezone.utc)
            except Exception as err:
                msg = f"to_datetime float conversion EXCEPTION: {repr(err)}"
                logger.exception(msg)
                dt = datetime.fromtimestamp(int(rec_time.split('.', 1)[0]), tz=timezone.utc)
        elif rec_time.isnumeric() and len(rec_time) == 13:
            dt = datetime.fromtimestamp((int(rec_time)/1000), tz=timezone.utc)
        else:
            dt = datetime.fromtimestamp(int(rec_time), tz=timezone.utc)
    elif isinstance(rec_time, int):
        if len(str(rec_time)) == 13:
            dt = datetime.fromtimestamp(rec_time/1000, tz=timezone.utc)
        else:
            dt = datetime.fromtimestamp(rec_time, tz=timezone.utc)
    elif isinstance(rec_time, float):
        dt =  datetime.fromtimestamp(rec_time, tz=timezone.utc)
    else:
        dt = rec_time
    return dt

def mtime_int(tm):
    return int(to_datetime(tm).timestamp() * 1000)

def mtime_str(tm):
    return str(int(to_datetime(tm).timestamp() * 1000))

### array de-duplication
def unique(*args):
    temp = []
    for l in args:
        if isinstance(l, (list, tuple)):
            for i in l:
                if i not in temp:
                    temp.append(i)
    return temp


def walk_parse(path, kpos, exclude=False, match=None):
    files = AD()
    for f in [f for f in shcmd(f'find {path} -type f')[0] if f != '']:
        fparts = f.split('/')
        k = fparts[kpos]
        rec = (f, '/'.join(fparts[0:kpos]), k, '_'.join(fparts[kpos:]))
        if match and isinstance(match, list) and len(match):
            take = False
            for m in match:
                if m in f:
                    take = True
                    break
        else:
            take = True
        if exclude and isinstance(exclude, list) and len(exclude):
            drop = False
            for m in exclude:
                if m in f:
                    drop = True
                    break
        else:
            drop = exclude

        if take and not drop:
            if k not in files:
                files[k] = [rec]
            else:
                files[k].append(rec)
    return files
