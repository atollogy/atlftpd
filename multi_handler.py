#!/usr/bin/env python3

from attribute_dict import *
from common import *
from log import *

import base64
import cv2
import concurrent.futures
from datetime import date, datetime, timedelta, timezone
from email import policy
from email.mime.application import MIMEApplication
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.parser import BytesParser
from email import encoders
import functools
import gzip
import http.client as httplib
import inotify.adapters
import iso8601
import json
import math
import mimetypes
import multiprocessing as mp
from multiprocessing import current_process, Pool, Queue
import numpy as np
from operator import itemgetter, attrgetter
import os
from os import devnull, kill
import paho.mqtt.client as mqtt
from pyftpdlib.handlers import FTPHandler
import queue
import re
import shutil
import signal
import subprocess as _sp
import sys
import threading
from threading import Thread
import time

import urllib3
urllib3.util.ssl_.DEFAULT_CIPHERS = 'ALL'
import requests
import requests.packages.urllib3.util.ssl_
requests.packages.urllib3.util.ssl_.DEFAULT_CIPHERS = 'ALL'


cpu_count = mp.cpu_count() - 1
FILE_HANDLERS = cpu_count if cpu_count > 2 else 2

RECEIVE_METRIC = 'atl.edge.atlftpd.receive_count'
SEND_METRIC = 'atl.edge.atlftpd.send_count'

FUNCTION_VERSION = (
    shcmd('git log -n1 -p -- {}| grep "^commit"'.format(os.path.abspath(__file__)))[0][0]
    .split(" ")[-1]
    .strip("\n")[0:8]
)


class ATLFTPD_Error(Exception):
    def __init__(self, message, status_code=400, payload=None):
        Exception.__init__(self)
        self.message = message
        self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv["message"] = self.message
        return rv

class GracefulInterruptHandler(object):
    def __init__(self, signals=(signal.SIGINT, signal.SIGTERM), funcs=[]):
        self.signals = signals
        self.original_handlers = {}
        self.funcs = funcs

    def __enter__(self):
        self.interrupted = False
        self.released = False

        for sig in self.signals:
            self.original_handlers[sig] = signal.getsignal(sig)
            signal.signal(sig, self.handler)

        return self

    def handler(self, signum, frame):
        [f() for f in self.funcs]
        self.release()
        self.interrupted = True

    def __exit__(self, type, value, tb):
        self.release()

    def release(self):
        if self.released:
            return False

        for sig in self.signals:
            signal.signal(sig, self.original_handlers[sig])

        self.released = True
        return True

class Backlog_Pool(concurrent.futures.ThreadPoolExecutor):
    def __init__(self, max_workers=FILE_HANDLERS, name='Backlog_Pool'):
        if name is None:
            name = threading.currentThread().getName()
        concurrent.futures.ThreadPoolExecutor.__init__(self, max_workers=max_workers*2, thread_name_prefix=f'Backlog_Pool-{name}')

class Forwarding_Pool(concurrent.futures.ThreadPoolExecutor):
    def __init__(self, max_workers=FILE_HANDLERS, name='Forwarding_Pool'):
        if name is None:
            name = threading.currentThread().getName()
        concurrent.futures.ThreadPoolExecutor.__init__(self, max_workers=max_workers, thread_name_prefix=f'Forwarding_Pool-{name}')

def back_log_resend():
    time.sleep(60)
    def send_to_api(blmeta):
        if not isinstance(blmeta.body , bytes):
            blmeta.body = blmeta.body.encode()
        try:
            resp = requests.post(
                url=blmeta.fmeta.uri,
                headers=blmeta.headers,
                data=blmeta.body,
                timeout=450
            )
            resp.raise_for_status()
            logger.info(f"back_log_resend: processing completed: file={blmeta.fmeta.bundle_json}")
            return True
        except Exception as x:
            logger.info(f"back_log_resend: API response status NOT OK {repr(x)} - {blmeta.fmeta.bundle_json}")
            time.sleep(0.1)
            return False

    def clean_backlog():
        files = [
            f for f in shcmd('''find /var/lib/atollogy/atlftpd/backlog -type f ! -name "*.json" -mmin +60 ''')[0]
            if f not in ['.', '..', './', '/'] and len(f) > 5 and '/opt/atlftpd' not in f
        ]
        # logger.info(f"cleaning backlog intermediate state files: {files}")
        for f in files:
            try:
                AD.load(f)
            except Exception as e:
                logger.exception(f"back_log_resend json load exception: {e}\nDeleting file: {f}")
                os.remove(f)
            finally:
                shcmd(f"mv {f} {f}.json")

    def backlog_forward(fl):
        logger.info(f"backlog_forward: file list {fl}")
        if not isinstance(fl, list):
            fl = [fl]

        for f in [str(f).strip() for f in fl
                  if len(f) and f not in ['.', '..', './', '/']
                  and '/opt/atlftpd' not in f and os.path.exists(f)]:
            try:
                logger.info("back_log_resend: processing file={}".format(f))
                blmeta = AD(persistTGT=f)
                if blmeta.fmeta.bundle_json != f:
                    blmeta.fmeta.bundle_json = f
            except Exception as e:
                logger.exception(
                    "back_log_resend json load exception: {}\nDeleting file: {}".format(
                        e, f
                    )
                )
                os.remove(f)
                return None

            try:
                if "btype" not in blmeta:
                    logger.info("blmeta does not contain btype - removing")
                    cleanup(blmeta.fmeta)
                    del blmeta
                    os.remove(f)
                    return

                logger.info(
                    "back_log_resend: processing file={} btype={}".format(
                        f, blmeta.btype
                    )
                )
                if blmeta.btype == "api":
                    try:
                        res = send_to_api(blmeta)
                        if res:
                            cleanup(blmeta.fmeta)
                            del blmeta
                        else:
                            update_backlog(blmeta)
                    except TypeError:
                        cleanup(blmeta.fmeta)
                        del blmeta
                elif blmeta.btype == "mvedge":
                    if blmeta.fmeta.step_function in ["locallpr", "annotate", "anonymize",
                                                      "gdpr", "gdpr_tf", "gdpr_f", "gdpr_coco",
                                                      "annotate_tf", "annotate_f", "annotate_coco",
                                                      "anonymize_tf", "anonymize_f",  "anonymize_coco",
                                                      "gdpr_cargo", "annotate_cargo", "anonymize_cargo",
                                                      "gdpr_person_at_cargo", "annotate_person_at_cargo", "anonymize_person_at_cargo"]:
                        try:
                            resp = requests.post(
                                url="http://127.0.0.1:7080/object_detection/{}".format(
                                    blmeta.fmeta.stepCfg.function
                                ),
                                headers=blmeta.headers,
                                json=blmeta.fmeta.stepCfg,
                                params={},
                                timeout=450
                            )
                            resp.raise_for_status()
                            logger.debug(f"back_log_resend: backlog MVAPI response: {resp}")
                            if 'gdpr' not in blmeta.fmeta.stepCfg.function:
                                oldstep = blmeta.fmeta.step_name
                                blmeta.fmeta.step_name = 'default'
                                res = send_to_api(blmeta)
                                if res:
                                    cleanup(blmeta.fmeta)
                                    del blmeta
                                else:
                                    blmeta.fmeta.step_name = oldstep
                                    update_backlog(blmeta)
                            else:
                                cleanup(blmeta.fmeta)
                                del blmeta
                                logger.info(f"back_log_resend: processing completed: file={f}")
                        except Exception as e:
                            logger.info(f"back_log_resend: MVAPI response status NOT OK - {repr(x)}")
                            update_backlog(blmeta)

                elif blmeta.btype == "mqtt":
                    res = shcmd(
                            '/usr/bin/mosquitto_pub -t atollogy/img -m "0:%(motion_name)s"' % (blmeta.fmeta)
                        )
                    if res[-1] == 0:
                        cleanup(blmeta.fmeta)
                        del blmeta
                    else:
                        update_backlog(blmeta)
                elif blmeta.btype == "s3":
                    res = shcmd(
                            "aws s3 mv %(input_filename)s s3://atl-%(env)s-%(cgr)s-rawdata/%(canonical_path)s/%(canonical_filename)s" % (blmeta.fmeta)
                        )
                    if res[-1] == 0:
                        cleanup(blmeta.fmeta)
                        del blmeta
                    else:
                        update_backlog(blmeta)
                else:
                    logger.info(
                        "back_log_resend: backlog file bundle unknown btype: {}".format(
                            blmeta.btype
                        )
                    )
                    cleanup(blmeta.fmeta)
                    del blmeta
                return
            except Exception as e:
                logger.exception("back_log_resend: exception: {}".format(e))
                update_backlog(blmeta)

    while True:
        try:
            clean_backlog()
            with Backlog_Pool() as pool:
                try:
                    files = [fn for fn in shcmd("find /var/lib/atollogy/atlftpd/backlog -type f -name '*.json'")[0] if fn][:1000]
                    logger.info("back_log_resend: number of files={}".format(len(files)))
                    pool.map(backlog_forward, files, chunksize=5, timeout=450)
                    logger.info("back_log_resend: sleep 450 seconds completed.")
                except Exception as err:
                    pool.shutdown(wait=False)
        except Exception as err:
            logger.error(f"back_log_resend exception: {repr(err)}")
        time.sleep(300)  # sleep for 15 minutes before sweeping backlog again


def fs_watcher(bcm, work_queue):
    BCM = bcm
    incoming_dir = BCM.svcs[BCM.scode].incoming_dir
    image_dir = BCM.svcs[BCM.scode].images_dir
    watcher = inotify.adapters.Inotify()
    watcher.add_watch(incoming_dir)

    def enqueue_file(fname):
        stub = fname[len(incoming_dir) + 1:]
        newfile = '/'.join([image_dir, stub])
        fparts = newfile.split('/')
        fqdnd = parse_fqdn(fparts[5], bcm=BCM)
        img_dir = newfile.rsplit('/', 1)[0]
        os.system(f'mkdir -p {img_dir}')
        os.rename(fname, newfile)
        work_queue.put(newfile)
        logger.info(f"fs_watcher.queue_add: file {newfile} into work queue at depth {work_queue.qsize()}")

    logger.info(f"fs_watcher starting to monitor directory:  {incoming_dir}")

    while True:
        try:
            for event in watcher.event_gen(yield_nones=False):
                (_, event_types, path, filename) = event
                fname = '/'.join([path, filename])
                if 'IN_CLOSE_WRITE' in event_types:
                    enqueue_file(fname)
        except Exception as err:
            print(f"exception caught in fs_watcher: {err}")

class MQTT_Watcher():
    watchers = AD()

    def __init__(self, bcm, image_dir='/var/lib/atollogy/images', incoming_dir='/var/lib/motion', qname='atollogy/rtsp', work_queue=None, handler='filename'):
        if qname in MQTT_Watcher.watchers:
            self = MQTT_Watcher.watchers[qname]
        else:
            MQTT_Watcher.watchers[qname] = self
            self.running = False
            self.bcm = bcm
            self.watch_queue = qname
            self.work_queue = work_queue
            self.image_dir = image_dir
            self.incoming_dir = incoming_dir
            if self.incoming_dir[-1] == '/':
                self.incoming_dir = self.incoming_dir[0:-1]
            self.client_name = f'atlftpd_{qname.replace("/", "_")}'

            self.watcher = mqtt.Client(client_id=self.client_name, clean_session=False, userdata=None, transport="tcp")
            self.watcher.on_connect = self.on_connect
            self.watcher.on_disconnect = self.on_disconnect
            self.watcher.on_message = handler if callable(handler) else getattr(self, f'on_message_{handler}')
            self.watcher.connect(host="127.0.0.1", port=1883)
            time.sleep(5)

    def run(self, *args, **kwargs):
        logger.info(f'MQTT_Watcher: starting MQTT Watcher thread - {self.watch_queue}')
        try:
            self.watcher.loop_forever()
        except Exception as err:
            logger.info(f'MQTT_Watcher error: {self.watch_queue} - {err}')

    def make_stub(self, fname):
        fname = fname.replace('_M_', '[M]').replace('_R_', '[R]')
        fparts = fname.split('/')
        current = 0

        for idx, fnp in enumerate(fparts):
            current = idx
            if fnp.count('_') == 2 and any([tenv in fnp for tenv in ['dev', 'stg', 'prd']]):
                break

        stub = '/'.join(fparts[current:])
        logger.debug(f"stub filename: {stub}")
        return stub

    def on_connect(self, client, userdata, flags, rc):
        self.watcher.subscribe(self.watch_queue, 1)

    def on_disconnect(self, client, obj, rc):
        self.watcher.reconnect()

    def on_message_filename(self, client, userdata, msg):
        logger.info(f"MQTT_Watcher.on_message - received message: {msg.topic} - {msg.payload}")
        fname = msg.payload.decode()
        if os.path.exists(fname):
            logger.info(f"MQTT_Watcher.on_message - filename exists: {fname}")
            stub = self.make_stub(fname)
            fqdnd = parse_fqdn(stub.split('/')[0], bcm=self.bcm)
            newfile = '/'.join([self.image_dir, stub])
            img_dir = newfile.rsplit('/', 1)[0]
            os.makedirs(img_dir, exist_ok=True)
            shutil.copyfile(fname, newfile)
            self.work_queue.put(newfile)
            logger.info(f"MQTT_Watcher.on_message - moved {fname} to {newfile} and added it to work queue at depth: {self.work_queue.qsize()}")
        else:
            logger.info(f"MQTT_Watcher.on_message - filename {fname} does not exist: skipping")

    def on_message_frigate_event(self, client, userdata, msg):
        try:
            logger.info(f"MQTT_Watcher.on_message - received frigate message: {msg.topic}")
            # frigate/<camera name>/events/[start|stop]
            event = json.loads(msg.payload.decode())

            camera = msg.topic.split('/')[1]
            base_fname = os.path.join(self.incoming_dir, camera + '-' + event['id'])
            video_fname = base_fname + '.mp4'
            metadata_fname = base_fname + '.json'

            if event['false_positive']:
                return

            logger.info(f'MQTT_Watcher.on_message - handling frigate event: {video_fname}')
            fqdn = parse_fqdn(camera, bcm=self.bcm)

            gwid = event.get('gateway_id', '000000000000')
            fps = event.get('fps', 5)

            start_time = datetime.fromtimestamp(event.get('start_time', time.time()))
            folder = os.path.join(
                self.image_dir, camera, gwid,
                'activation', 'frigate',
                start_time.strftime('%Y-%m-%d'),
                start_time.strftime('%H'),
                start_time.strftime('%M')
            )
            newfile = os.path.join(folder, f'{start_time.strftime("%S")}_{fps}[M].mp4')
            os.makedirs(folder, exist_ok=True)

            # TODO: async alternative to spinlock
            # for now this works since we are in a different thread and it should let
            # frigate catch up (so retry shouldn't always take so long)
            retry_counter = 0
            while not os.path.exists(video_fname):
                if retry_counter > 100:
                    logger.error(f'frigate event missing file {camera}, {video_fname}')
                    return
                time.sleep(0.5)
                retry_counter += 1

            os.rename(video_fname, newfile)
            os.remove(metadata_fname)   # remove json stored version of message

            self.work_queue.put(newfile)
            logger.info(f"MQTT_Watcher.on_message - moved {video_fname} to {newfile} and added it to work queue at depth: {self.work_queue.qsize()}")
        except Exception as err:
            logger.info(f'MQTT_Watcher.on_message_frigate: {err}')

def cleanup(fmeta):
    try:
        if fmeta:
            for fk in ["bundle_name", "bundle_json", "input_filename", "motion_name"]:
                if fk in fmeta and fmeta[fk] and os.path.exists(fmeta[fk]) and '/opt/atlftpd' not in fmeta[fk]:
                    logger.info(f"Cleanup is removing file: {fmeta[fk]}")
                    shcmd(f"rm -rf {fmeta[fk]}")
            if 'files' in fmeta:
                for f in fmeta.files:
                    if os.path.exists(f):
                        logger.info(f"Cleanup is removing file: {f}")
                        shcmd(f"rm -rf {f}")
            del fmeta
        else:
            logger.info(f"Cleanup received null fmeta")
    except Exception as err:
        logger.error(f"cleanup exception: {repr(err)}")
        if fmeta and  'bundle_json' in fmeta and os.path.exists(fmeta.bundle_json):
            shcmd(f"rm -rf {fmeta.bundle_json}")
            logger.error(f"cleanup deleted: {fmeta.bundle_json}")
        else:
            logger.error(f"cleanup - exception causing fmeta could not be found. Nothing to do.")

def update_backlog(blmeta):
    try:
        if "backlog_attempts" not in blmeta:
            # backlog attempt = 300 secs
            blmeta.backlog_attempts = 1

        # Delete backlog more than ~7 days old as counted by backlog attempts every ~ 5 min
        if blmeta.backlog_attempts >= 100:
            cleanup(blmeta.fmeta)
            del blmeta
        else:
            blmeta.backlog_attempts += 1
            blmeta.sync()
    except Exception as err:
        logger.error(f"update_backlog exception: {repr(err)}")
        cleanup(blmeta.fmeta)
        del blmeta

def dd_metric(bcm, nodename, metric, value):
    fqdnd = parse_fqdn(nodename, bcm=bcm)
    payload = {
        'series': [
            {
                'metric': metric,
                'points': [[time.time(), value]],
                'interval': 1,
                'type': 'count',
                'host': bcm.fqdn,
                'tags': [
                    f"env:{fqdnd.env}",
                    f"cgr:{fqdnd.cgr}",
                    f"scode:{fqdnd.scode}",
                    f"nodename:{nodename}"
                ]
            }
        ]
    }
    requests.post('https://app.datadoghq.com/api/v1/series?api_key=d6ace30b26f1051a0873f79e5df801af', json=payload)
    logger.info(f"dd_metric: sent metric {payload}")

class File_Forwarder():
    def __init__(self, bcm, send_queue):
        self.BCM = bcm
        self.send_queue = send_queue
        self.forwarding_mode = "direct"
        self.forwarding_role = "core"
        self.timeout = 65536
        self.check_time = 0
        self.avail = 0
        self.pool = Forwarding_Pool()

    def run(self, *args, **kwargs):
        logger.info('Starting File_Forwarding thread')
        with GracefulInterruptHandler(funcs=[self.pool.shutdown]) as GIH:
            while True:
                try:
                    if not self.send_queue.empty():
                        payload = self.send_queue.get(False)
                        if payload:
                            payload = AD.loads(payload)
                            logger.info(f"File_Forwarder.run_loop: forwarding filename: {payload.canonical_filename}")
                            self.pool.map(self.forward_file, [payload], timeout=450)
                    else:
                        time.sleep(0.25)
                except queue.Empty:
                    time.sleep(0.25)
                    pass
                except Exception as err:
                    logger.exception(f"File_Forwarder.run_loop error: {err}")

    def backlog(self, bundle_name):
        logger.info("backlogging bundle: {}".format(bundle_name))
        shcmd("mv {} {}.json".format(bundle_name, bundle_name))
        return f"{bundle_name}.json"

    def make_bundle(self, btype, fmeta, path=None, headers=None, body=None):
        if btype not in ["api", "mqtt", "mvedge", "s3"]:
            return None
        try:
            bundle = AD(persistTGT=fmeta.bundle_name)
            bundle.bundle_name = fmeta.bundle_name
            bundle.btype = btype
            bundle.fmeta = fmeta
            bundle.headers = headers
            bundle.path = path
            bundle.body = body
            bundle.sync()
            return bundle
        except Exception as err:
            cleanup(fmeta)
            logger.exception(f"File_Forwarder.make_bundle exception: {err} - {fmeta}")
            return None

    def forward_to_api(self, fmeta):
        try:
            logger.info("Forwarding file {} to core".format(fmeta.canonical_filename))
            if fmeta.gateway_id is None:
                raise ATLFTPD_Error("Gateway ID is none")
            if fmeta.ext == 'mp4' and '_video' not in fmeta.step_name:
                fmeta.step_name = f"{fmeta.step_name}_video"
            im = index_metadata = AD()
            im.cameraId = 0
            im.collectionTime = im.collection_time = fmeta.collection_time
            im.gatewayId = im.gateway_id = fmeta.gateway_id
            im.nodename = fmeta.nodename
            im.steps = [AD()]

            im.steps[0].name = fmeta.step_name
            im.steps[0].function = fmeta.step_function
            im.steps[0].functionVersion = FUNCTION_VERSION
            im.steps[0].configVersion = "default"
            im.steps[0].collectionInterval = fmeta.collection_interval
            im.steps[0].inputParameters = {}
            im.steps[0].startTime = fmeta.startTime
            im.steps[0].endTime = fmeta.endTime if 'endTime' in fmeta else None
            im.steps[0].output = AD()
            im.steps[0].output.cause = im.steps[0].function

            logger.info(f"{fmeta.input_filename}: {im.steps[0]}")
            multipart_msg = MIMEMultipart("related")
            mime_msg = MIMEApplication(index_metadata.dumps(), _subtype="json")
            mime_msg[
                "Content-ID"
            ] = "<%(gateway_id)s/0/%(collection_time)s/index@at0l.io>" % (fmeta)
            multipart_msg.attach(mime_msg)

            try:
                if os.path.exists(fmeta.input_filename):
                    with open(fmeta.input_filename, "rb") as fobj:
                        ctype, encoding = mimetypes.guess_type(fmeta.input_filename)
                        if ctype is None:
                            ctype = 'application/octet-stream'
                        maintype, subtype = ctype.split('/', 1)

                        if maintype == 'text':
                            media = MIMEText(fobj.read(), _subtype=subtype)
                        elif maintype == 'image':
                            media = MIMEImage(fobj.read(), _subtype=subtype)
                        else:
                            media = MIMEBase(maintype, subtype)
                            media.set_payload(fobj.read())
                            encoders.encode_base64(media)

                        media[
                            "Content-Disposition"
                        ] = 'attachment; filename="%(step_name)s/%(filename)s"' % (fmeta)
                        media["Content-ID"] = (
                            "<%(gateway_id)s/0/%(collection_time)s/%(step_name)s/%(filename)s@at0l.io>"
                            % (fmeta)
                        )
                        multipart_msg.attach(media)

            except FileNotFoundError as err:
                logger.exception("file not found: {}".format(fmeta.input_filename))
                return None

            full_msg_string = multipart_msg.as_string(
                policy=multipart_msg.policy.clone(max_line_length=None, linesep="\r\n")
            )
            boundary = multipart_msg.get_boundary()

            # remove the generated document headers, since we manually generate the request headers
            body = re.sub(
                r"^[\S\s]*?--" + re.escape(boundary), r"--" + boundary, full_msg_string
            )

            headers = {
                "Content-Type": 'multipart/related; boundary="{}"'.format(boundary),
                "collection_time": str(fmeta.collection_time),
                "customer_id": fmeta.cgr,
                "Gateway_Id": fmeta.gateway_id,
                "nodename": fmeta.nodename
            }

            try:
                self.make_bundle("api", fmeta, path=fmeta.path, headers=headers, body=body)
            except ATLFTPD_Error as err:
                # if the metadata cannot be persisted, it makes no sense in trying to
                # save anything because the on disk bundle is our principal safety net
                cleanup(fmeta)
                logger.exception(f"File_Forwarder.forward_to_api make bundle exception {err} - {fmeta}")
                return (err, err, 1, fmeta.bundle_name)
            try:
                resp = requests.post(
                    url=fmeta.uri,
                    headers=headers,
                    data=body,
                    timeout=450
                )
                resp.raise_for_status()
                logger.info(f"Forwarding {fmeta.canonical_filename}: api raw response: {resp}")
                msg = f"File_Forwarder.forward_to_api - forwarding {fmeta.canonical_filename}: api raw response: {resp}"
                logger.info(msg)
                return (resp, None, 0, None)
            except Exception as x:
                msg = f"File_Forwarder.forward_to_api exception {repr(x)} - {fmeta}"
                logger.exception(msg)
                time.sleep(0.25)
                return (None, msg, 1, self.backlog(fmeta.bundle_name))
        except Exception as err:
            msg = f"File_Forwarder.forward_to_api exception {err} - {fmeta}"
            logger.exception(msg)
            return (err, f"ERROR: {err}", 1, None)

    def forward_to_aws_s3(self, fmeta):
        # AWS forwarder filenames
        logger.info("aws s3 forwarding file {}".format(fmeta.filename))
        try:
            self.make_bundle("s3", fmeta)
            resp = shcmd(f"aws s3 mv {fmeta.input_filename} {fmeta.s3_path}")
            if resp[-1] == 0:
                return (resp, None, 0, None)
            else:
                return (None, resp, 1, self.backlog(fmeta.bundle_name))
        except Exception as err:
            logger.exception(f"File_Forwarder.forward_to_aws_s3 exception {err} - {fmeta}")
            return (err, "ERROR: {}".format(err), 1, None)

    def forward_file(self, fmeta):
        if isinstance(fmeta, str):
            fmeta = AD.loads(fmeta)
        if not isinstance(fmeta, AD):
            fmeta = AD(fmeta)
        logger.info(f"File_Forwarder.forward_file forwarding new file: {fmeta.canonical_filename}")

        if self.forwarding_mode == "mqtt":
            res = self.forward_to_mqtt(fmeta)
        elif self.forwarding_mode == "s3":
            res = self.forward_to_aws_s3(fmeta)
        else:
            # logger.info("step_function: {}".format(fmeta.step_function))
            if fmeta.step_function in ["locallpr", "annotate",
                "gdpr", "gdpr_tf", "gdpr_f", "gdpr_coco",
                "annotate_tf", "annotate_f", "annotate_coco",
                "anonymize_tf", "anonymize_f",  "anonymize_coco",
                "gdpr_cargo", "annotate_cargo", "anonymize_cargo",
                "gdpr_person_at_cargo", "annotate_person_at_cargo", "anonymize_person_at_cargo"]:
                try:
                    res = self.forward_to_mvedge(fmeta)
                except Exception as err:
                    logger.exception("File_Forwarder.forward_file forwarding exception: {}".format(err))

                if 'gdpr' not in fmeta.step_function:
                    fmeta.step_name = 'default'
                    res = self.forward_to_api(fmeta)
            else:
                res = self.forward_to_api(fmeta)

        if res and res[0] is not None:
            logger.info(f"File_Forwarder.forward_file successfully handled - {fmeta.bundle_name}")
            dd_metric(self.BCM, fmeta.nodename, SEND_METRIC, 1)
            cleanup(fmeta)
        else:
            logger.error(f"File_Forwarder.forward_file - Failed to send payload, backlog created: {fmeta.bundle_json}")

    def forward_to_mqtt(self, fmeta):
        # MQTT forwarder filenames
        logger.info("mqtt forwarding file {}".format(fmeta.filename))
        try:
            self.make_bundle("mqtt", fmeta)
            os.rename(fmeta.input_filename, fmeta.motion_name)
            res = shcmd(
                '/usr/bin/mosquitto_pub -t atollogy/img -m "0:%(motion_name)s"' % (fmeta)
            )
            if res[-1] == 0:
                cleanup(fmeta)
                return tuple(list(res) + [None])
            else:
                return tuple(list(res) + [self.backlog(fmeta.bundle_name)])
        except Exception as err:
            # if some other exception is thrown and caught here, we have no recourse other than logging the event
            logger.exception(f"File_Forwarder.forward_to_mqtt exception {err} - {fmeta}")
            cleanup(fmeta)
            return (None, "ERROR: {}".format(err), 1, None)

    def forward_to_mvedge(self, fmeta):
        if fmeta.ext == 'mp4':
            logger.error("File_forwarder.forward_to_mvedge ERROR cannot forward video file to mvedge: {}".format(fmeta.filename))
            return
        logger.info("mvedge forwarding file {}".format(fmeta.filename))
        try:
            step_cfg = fmeta.stepCfg
            hdrs = {
                "Content-Type": "application/json",
                "gateway_id": step_cfg.gateway_id,
                "nodename": step_cfg.nodename,
                "customer_id": step_cfg.customer_id,
                "Connection": "close"
            }

            # logger.info(fmeta.input_filename)
            fdata = np.asarray(
                bytearray(open(fmeta.input_filename, "rb").read()), dtype=np.uint8
            )
            to_send = fdata.tobytes()

            for k in ["filedata", "filelist", "input_image_indexes"]:
                if k in step_cfg:
                    del step_cfg[k]

            step_cfg.image = base64.b64encode(to_send).decode()

            try:
                self.make_bundle(
                    "mvedge", fmeta, path=step_cfg.function, headers=hdrs, body=step_cfg
                )
            except ATLFTPD_Error as err:
                # if the metadata cannot be persisted, it makes no sense in trying to
                # save anything because the on disk bundle is our principal safety net
                cleanup(fmeta)
                return (None, err, 1, fmeta.bundle_name)

            try:
                resp = requests.post(
                    url="http://127.0.0.1:7080/object_detection/{}".format(
                        step_cfg.function
                    ),
                    headers=hdrs,
                    json=step_cfg,
                    params={},
                    timeout=450
                )
                resp.raise_for_status()
                return (resp, None, 0, None)
            except Exception as x:
                msg = f"File_Forwarder.forward_to_mvapi exception {repr(x)} - {fmeta.input_filename}"
                logger.exception(msg)
                return (None, msg, 1, self.backlog(fmeta.bundle_name))

        except Exception as err:
            logger.exception(f"File_Forwarder.forward_to_mvapi exception {err} - {fmeta}")
            return (None, "ERROR: {}".format(err), 1, self.backlog(fmeta.bundle_name))


class File_Worker():
    normal_fields_jpg = ['var', 'lib', 'atollogy', 'images', 'nodename', 'gateway_id', 'date', 'event_type', 'ftype', 'hour', 'minute', 'fname']
    short_normal_fields_jpg = ['var', 'lib', 'atollogy', 'images', 'nodename', 'gateway_id', 'date', 'event_type', 'fname']
    ext_fields_jpg = ['var', 'lib', 'atollogy', 'images', 'nodename', 'step_name', 'step_function', 'gateway_id', 'date', 'event_type', 'ftype', 'hour', 'minute', 'fname']
    short_ext_fields_jpg = ['var', 'lib', 'atollogy', 'images', 'nodename', 'step_name', 'step_function', 'gateway_id', 'date', 'event_type', 'fname']
    motion_fields_jpg = ['var', 'lib', 'atollogy', 'images', 'nodename', 'gateway_id', 'event_type', 'src',  'date', 'hour', 'minute', 'fname']
    ext_motion_fields_jpg = ['var', 'lib', 'atollogy', 'images', 'nodename', 'gateway_id', 'step_name', 'step_function', 'event_type', 'src',  'date', 'hour', 'minute', 'fname']
    frame_extraction_fields = ['var', 'lib', 'atollogy', 'images', 'nodename', 'step_name', 'step_function', 'gateway_id', 'date', 'event_type', 'hour', 'minute', 'fname']

    normal_fields_video = ['var', 'lib', 'atollogy', 'images', 'nodename', 'gateway_id', 'date', 'event_type', 'ftype', 'hour', 'fname']
    short_normal_fields_video = ['var', 'lib', 'atollogy', 'images', 'nodename', 'gateway_id', 'date', 'event_type', 'fname']
    ext_fields_video = ['var', 'lib', 'atollogy', 'images', 'nodename', 'step_name', 'step_function', 'gateway_id', 'date', 'event_type', 'ftype', 'hour', 'fname']
    short_ext_fields_video = ['var', 'lib', 'atollogy', 'images', 'nodename', 'step_name', 'step_function', 'gateway_id', 'date', 'event_type', 'fname']
    motion_fields_video = ['var', 'lib', 'atollogy', 'images', 'nodename', 'gateway_id', 'event_type', 'src',  'date', 'hour', 'minute', 'fname']
    ext_motion_fields_video = ['var', 'lib', 'atollogy', 'images', 'nodename', 'gateway_id', 'step_name', 'step_function', 'event_type', 'src',  'date', 'hour', 'minute', 'fname']
    frigate_fields_video = ['var', 'lib', 'atollogy', 'images', 'nodename', 'gateway_id', 'event_type', 'src',  'date', 'hour', 'minute', 'fname']
    ext_frigate_fields_video = ['var', 'lib', 'atollogy', 'images', 'nodename', 'gateway_id', 'step_name', 'step_function', 'event_type', 'src',  'date', 'hour', 'minute', 'fname']
    clip_extraction_fields = ['var', 'lib', 'atollogy', 'images', 'nodename', 'step_name', 'step_function', 'gateway_id', 'date', 'event_type', 'fname']

    acti_b_class_fields = ['var', 'lib', 'atollogy', 'images', 'fpath', 'fname']
    acti_e_class_fields = ['var', 'lib', 'atollogy', 'images', 'nodename', 'step_name', 'step_function', 'fname']
    acti_z_class_fields = ['var', 'lib', 'atollogy', 'images', 'nodename', 'gateway_id', 'step_name', 'step_function', 'fname']

    vc_fields_v1 = ['vc_profile', 'vc_mode', 'separate_heartbeat', 'threshold', 'fxinterval', 'transcode_frame_rate', 'scene_bridge', 'max_frames']
    vc_fields_v2 = ['vc_mode', 'return_src', 'separate_heartbeat', 'threshold', 'fxinterval', 'transcode_frame_rate', 'scene_bridge', 'max_scene_length', 'max_frames']

    event_types = AD({
        0: 'default',
        1: 'scheduled',
        2: 'activation'
    })

    motion_sources = ['rtsp', 'local', 'video0', 'video1', 'video2', 'video3']
    frigate_sources = ['frigate']

    def __init__(self, bcm, send_queue):
        self.BCM = bcm
        self.send_queue = send_queue
        self.forwarding_mode = "direct"
        self.forwarding_role = "core"
        self.timeout = 65536
        self.check_time = 0
        self.avail = 0

    def backlog(self, bundle_name):
        logger.info("backlogging bundle: {}".format(bundle_name))
        shcmd("mv {} {}.json".format(bundle_name, bundle_name))
        return f"{bundle_name}.json"

    def close_subprocess(self, process):
        if not process:
            return
        try:
            process.terminate()
            process.wait(3)
            if process.returncode is None:
                kill(process.pid, 9)
        except:
            kill(process.pid, 9)

    def compare_images(self, img1, img2):
        diff = np.sum((img1.astype("float") - img2.astype("float")) ** 2)
        diff /= float(img1.shape[0] * img2.shape[1])
        return diff

    def construct_time(self, fmeta):
        td = AD()
        for t in ['year', 'month', 'day', 'hour', 'minute', 'second', 'microsecond']:
            if t in fmeta:
                val = fmeta[t]
            elif t == 'microsecond':
                val = 0
            else:
                val = getattr(fmeta.now, t)
            if t == 'microsecond':
                td[t] = '{0:06d}'.format(int(val))
            elif t == 'year':
                td[t] = '{0:04d}'.format(int(val))
            else:
                td[t] = '{0:02d}'.format(int(val))
        return to_datetime(
            f"""{'-'.join([td.year, td.month, td.day])} {':'.join([td.hour, td.minute, td.second])}.{td.microsecond}+0000"""
        )

    def create_filename(self, fmeta, fcount=None, fnum=None, ftype='frame', scene=None, sid=0):
        fpath = f"/var/lib/atollogy/images/{fmeta.nodename}/{fmeta.step_name}/{fmeta.step_function}/{fmeta.gateway_id}/{fmeta.date}/"
        if ftype == 'clip':
            fpath += f"{fmeta.event_type}/"
            fname = f"{fmeta.now.hour}.{fmeta.now.minute}.{fmeta.now.second}-"
            fname += f"{fmeta.end_time.hour}.{fmeta.end_time.minute}.{fmeta.end_time.second}[CX]"
            fname += f"[{sidx}:{scene[0][0]}]"
            fname += f"[{sidx}:{scene[-1][0]}][{sid}].mp4"
            return fpath, fname


        elif ftype == 'frame':
            fpath += "{}/{}/{}/".format(fmeta.event_type,
                                        '{0:02d}'.format(fmeta.now.hour),
                                        '{0:02d}'.format(fmeta.now.minute))
            fname = "{}.{}[FX][{}@{}][{}].jpg".format(
                                                '{0:02d}'.format(fmeta.now.second),
                                                '{0:04d}'.format(fmeta.now.microsecond),
                                                fcount,
                                                fnum,
                                                sid
                                            )
        elif ftype == 'region':
            if fmeta.ext == 'jpg':
                fpath += "001/jpg/{}/{}/".format('{0:02d}'.format(fmeta.now.hour),
                                             '{0:02d}'.format(fmeta.now.minute))
            else:
                fpath += 'video_001/'
            fname = fmeta.input_filename.rsplit('/', 1)[-1]
        else:
            fpath, fname = fmeta.input_filename.rsplit('/', 1)
            fpath += '/'

        return fpath, fname

    def convert_to_mp4(self, fmeta):
        try:
            if fmeta.ext not in ['dav', 'dav_', 'raw']:
                return fmeta
            timeout = 900
            running_time = 0
            mp4_filename = fmeta.input_filename.rsplit('.', 1)[0] + '.mp4'
            command = '/usr/bin/nice -n10 /usr/bin/ffmpeg -y -hide_banner -loglevel fatal '
            command += '-flags low_delay -strict experimental '
            command += f'-fflags genpts -fflags discardcorrupt -i {fmeta.input_filename} '
            command += f'-c:v libx264 -crf 24 -filter:v fps=fps={fmeta.transcode_frame_rate} {mp4_filename}'
            logger.info("STARTING - mp4 video conversion of file {}".format(fmeta.input_filename))
            logger.info(f"Using command: {command}")
            oldfmeta = AD(fmeta)
            oldfmeta.step_name = f"{fmeta.step_name}_video"
            with open(devnull, "wb") as dev_null:
                start = datetime.utcnow().timestamp()
                process = _sp.Popen(command.split(' '), stdout=_sp.PIPE, stderr=_sp.PIPE)
                while self.subprocess_is_running(process) and running_time <= timeout:
                    time.sleep(1)
                    running_time += 1
                else:
                    if self.subprocess_is_running(process):
                        self.close_subprocess(process)
                        shcmd(f"rm -rf {oldfmeta.input_filename}")
                        logger.error("ERROR: mp4 video conversion of file {} exceeded timeout or failed".format(oldfmeta.input_filename))
                        return None
                cmd_time = datetime.utcnow().timestamp() - start
                print(f"conversion execution time: {cmd_time}")

                (cmdout, cmderr) = process.communicate()
                print(f"conversion stdput: {cmdout}")
                print(f"conversion stderr: {cmderr}")
                if os.path.exists(mp4_filename):
                    shcmd(f"rm -rf {oldfmeta.input_filename}")
                    fmeta = self.generate_file_metadata(mp4_filename, oldfmeta.now, oldfmeta=oldfmeta)
                    fmeta.origin = True
                    logger.info("mp4 video conversion successful - new fmeta: {}".format(fmeta.jstr()))
                    return fmeta
                else:
                    shcmd(f"rm -rf {oldfmeta.input_filename}")
                    logger.error(f"ERROR: mp4 video conversion of file {oldfmeta.input_filename} failed with exit code {process.returncode}\n    stdout: {cmdout}\n    stderr: {cmderr}")
                    return None
        except Exception as err:
            logger.exception(f"File_Worker.convert_to_mp4 exception occurred handling: {fmeta.input_filename}- {repr(err)}")
            return None

    def do_scene_detection(self, fmeta):
        '''
        Creates a list of timestamps that will hopefully correspond to scene changes.
        '''
        DEFAULT_SCENE_TERMINATION = 2 # default 2
        try:
            if not os.path.exists(fmeta.input_filename):
                logger.error(f"ERROR: Scene detection cannot start - missing source file: {fmeta.input_filename}")
                return

            logger.info(f"Beginning scene detection on: {fmeta.input_filename}")
            cmd = [
                '/usr/bin/ffprobe', '-preset', 'ultrafast', '-f', 'lavfi',  '-i', f'movie=\'{fmeta.input_filename}\',signalstats',
                '-show_entries', 'frame=pkt_pts_time,pict_type:frame_tags=lavfi.signalstats.YDIF',
                '-of', 'json'
            ]

            # logger.info(f"ffprobe command: {' '.join(cmd)}")
            start = datetime.utcnow().timestamp()
            ydiff_json = _sp.check_output(cmd)
            raw_ydiffs = json.loads(ydiff_json)
            cmd_time = datetime.utcnow().timestamp() - start
            print(f"ffprobe execution time: {cmd_time}")

            ydiff_list = []
            fmeta.vclock_time = False
            fmeta.frames = []
            fnum = 1
            for i in raw_ydiffs:
                for x in raw_ydiffs[i]:
                    val = float(x['tags']['lavfi.signalstats.YDIF'])
                    ydiff_list.append(val)
                    fmeta.frames.append((fnum, float(x["pkt_pts_time"]), x['pict_type'], val))
                    fnum += 1
                    if fmeta.vclock_time is None:
                        fmeta.vclock_time = float(x["pkt_pts_time"])

            fmeta.total_frames = len(fmeta.frames)
            msg = f"{fmeta.nodename} - {fmeta.input_filename}:\n"
            ydiff_list = sorted(ydiff_list)
            oyd_max_diff = max(ydiff_list)
            oyd_avg = sum(ydiff_list)/len(ydiff_list)
            oyd_len = len(ydiff_list)
            oyd_std = math.sqrt(sum([(v - oyd_avg) ** 2 for v in ydiff_list])/oyd_len)
            msg += f"\toriginal: oyd_max_diff: {oyd_max_diff}, oyd_avg: {oyd_avg}, oyd_len: {oyd_len}, oyd_std: {oyd_std} - {ydiff_list}\n"

            ydiff_list = ydiff_list[:int(oyd_len * 0.98)]
            yd_avg = sum(ydiff_list)/len(ydiff_list)
            yd_len = len(ydiff_list)
            yd_std = math.sqrt(sum([(v - yd_avg) ** 2 for v in ydiff_list])/yd_len)
            yd_max_diff = max(ydiff_list)
            msg += f"\tfiltered: yd_max_diff: {yd_max_diff}, yd_avg: {yd_avg}, yd_len: {yd_len}, yd_std: {yd_std} - {ydiff_list}\n"

            fmeta.activation_threshold = fmeta.threshold * yd_max_diff
            if fmeta.activation_threshold < 1.50:
                fmeta.activation_threshold = 1.50
            msg += f"\tActivation threshold: {fmeta.activation_threshold}"
            logger.info(msg)

            fmeta.scenes = AD()
            for xm in fmeta.extraction_modes:
                if xm == 'reverse':
                    temp_frames = fmeta.frames[::-1]
                else:
                    temp_frames = fmeta.frames[:]
                mode_frames = []
                scene_length = len(temp_frames) - 1
                for x, fs in enumerate(temp_frames):
                    if x < scene_length:
                        if  fs[-1] <= temp_frames[x + 1][-1]:
                            mode_frames.append(fs)
                scenes = []
                scene_start = 0
                bridge = []
                do_bridging = fmeta.scene_bridge > 0
                activation_threshold = fmeta.activation_threshold
                num_scenes = 0
                num_scene_breaks = 0
                for fm in mode_frames:
                    fnum, frame_time, ftype, ydiff = fm
                    if fmeta.threshold_decay is not None and ((len(scenes) - num_scene_breaks - 1) > num_scenes):
                        activation_threshold = fmeta.activation_threshold - ((fmeta.activation_threshold * fmeta.threshold_decay) * (len(scenes) - num_scene_breaks))
                        num_scenes += 1
                    activate = (ydiff >= activation_threshold)
                    if do_bridging:
                        bridging = len(bridge)
                        if xm == 'reverse':
                            end_bridge = ((bridge[0][1] - frame_time) > fmeta.scene_bridge) if bridging else False
                        else:
                            end_bridge = ((frame_time - bridge[0][1]) > fmeta.scene_bridge) if bridging else False
                        if end_bridge:
                            scene_start = 0
                            bridging = 0
                            bridge = []
                        if activate:
                            if not scene_start:
                                scene_start = frame_time
                                scenes.append([fm])
                            elif bridging:
                                scenes[-1].extend(bridge)
                                bridge = []
                                scenes[-1].append(fm)
                            else:
                                scenes[-1].append(fm)
                        elif scene_start:
                            bridge.append(fm)
                    else:
                        if xm == 'reverse':
                            scene_too_long = ((scene_start - frame_time) > fmeta.max_scene_length) if scene_start else False
                        else:
                            scene_too_long = ((frame_time - scene_start) > fmeta.max_scene_length) if scene_start else False
                        if activate:
                            if not scene_start or scene_too_long:
                                scene_start = frame_time
                                scenes.append([fm])
                                if scene_too_long:
                                    num_scene_breaks += 1
                                    logger.info(f'Scene exceeded max duration: {fmeta.max_scene_length}')
                            else:
                                scenes[-1].append(fm)
                        else:
                            scene_start = 0

                # filter scenes: they must be at least 3 frames in length
                fmeta.scenes[xm] = [scn for scn in scenes if len(scn) >= 5]

            if len(fmeta.scenes[xm]) > 0:
                logger.info(f"{fmeta.nodename} - Scene Detection Number of Scenes Detected: {len(fmeta.scenes[xm])}")
                for i, x in enumerate(fmeta.scenes[xm]):
                    logger.info(f"Scene Detection - mode {xm} scene #{i} length: {len(x)}")
        except Exception as err:
            logger.exception(f"File_Worker.do_scene_detection exception occurred handling: {fmeta.input_filename}- {repr(err)}")
        return

    def expand_step_parameters(self, fmeta):
        fparts = fmeta.input_filename.split('/')
        def v1_vc_profile(fmeta, profile):
            fmeta.vcp = AD(dict(zip(self.vc_fields_v1, profile)))
            fmeta.update(fmeta.vcp)
            fmeta.scene_bridge = int(fmeta.scene_bridge)
            if fmeta.scene_bridge < 0:
                fmeta.max_scene_length = abs(fmeta.scene_bridge)
                fmeta.scene_bridge = 0
            else:
                fmeta.max_scene_length = 0
            if fmeta.step_name == 'lpc':
                fmeta.separate_heartbeat = 1
            else:
                fmeta.separate_heartbeat = 0

            fmeta.fxinterval = fmeta.fps = abs(int(fmeta.fxinterval))
            fmeta.threshold = int(fmeta.threshold)/100
            fmeta.transcode_frame_rate = int(fmeta.transcode_frame_rate)
            fmeta.scene_bridge = int(fmeta.scene_bridge)
            fmeta.max_frames = int(fmeta.max_frames)

        def v2_vc_profile(fmeta, profile):
            fmeta.vcp = AD(dict(zip(self.vc_fields_v2, profile)))
            fmeta.update(fmeta.vcp)
            fmeta.max_frames = int(fmeta.max_frames)
            if fmeta.max_frames < 0:
                fmeta.block_frame_extraction = True
            fmeta.scene_bridge = int(fmeta.scene_bridge)
            fmeta.threshold = float(int(fmeta.threshold)/100)
            fmeta.transcode_frame_rate = int(fmeta.transcode_frame_rate)
            fmeta.fxinterval = fmeta.fps = int(fmeta.fxinterval)

        if fmeta.step_name is not None and fmeta.step_function is not None:
            if fmeta.step_name in self.BCM.svcs.atlftpd.vc_profiles:
                if fmeta.step_function in self.BCM.svcs.atlftpd.vc_profiles[fmeta.step_name]:
                    if len(self.BCM.svcs.atlftpd.vc_profiles[fmeta.step_name][fmeta.step_function]) == len(self.vc_fields_v1):
                        v1_vc_profile(fmeta, self.BCM.svcs.atlftpd.vc_profiles[fmeta.step_name][fmeta.step_function])
                    else:
                        v2_vc_profile(fmeta, self.BCM.svcs.atlftpd.vc_profiles[fmeta.step_name][fmeta.step_function])
            elif len(fmeta.step_function.split('_')) == 7:
                v1_vc_profile(fmeta, [fmeta.step_function] + fmeta.step_function.split('_'))
            elif len(fmeta.step_function.split('_')) == 9:
                v2_vc_profile(fmeta, fmeta.step_function.split('_'))

            if fmeta.step_function.lower()[0] == 'b':
                fmeta.extraction_modes = ['forward', 'reverse']
            elif fmeta.step_function.lower() == 'r':
                fmeta.extraction_modes = ['reverse']
            else:
                fmeta.extraction_modes = ['forward']

        if not fmeta.step_name or fmeta.step_name == 'None':
            fmeta.step_name = 'atlftpd'

        if 'max_scene_length' in fmeta:
            if fmeta.max_scene_length < 0:
                fmeta.block_clip_extraction = True
                fmeta.collection_interval = 0
            else:
                fmeta.collection_interval = fmeta.max_scene_length
        elif fmeta.step_name == 'lpc':
            fmeta.collection_interval = fmeta.max_scene_length = 4
        else:
            fmeta.collection_interval = fmeta.max_scene_length = 60

        if fmeta.ext == 'jpg' and fmeta.is_scheduled and fmeta.separate_heartbeat:
            fmeta.step_name = 'heartbeat'
            fmeta.step_function = 'capture'
            fmeta.event_type = 'scheduled'
        elif '[M]' in fparts[-1]:
            fmeta.event_type = 'activation'
        elif fmeta.event_type.isnumeric() and int(fmeta.event_type) in self.event_types:
            fmeta.event_type = self.event_types[int(fmeta.event_type)]
        else:
            fmeta.event_type = 'default'

        if f'_customer_cfg.devices.gateways.{fmeta.nodename}.edge_routing' in self.BCM:
            edge_routing = self.BCM[f'_customer_cfg.devices.gateways.{fmeta.nodename}.edge_routing']
            if 'overrides' in edge_routing:
                fmeta.update(edge_routing.overrides)

    def extract_regions(self, fmeta):
        fmeta_results = []
        base_fmeta = AD(fmeta)
        step_cfg = base_fmeta.step_cfg
        del base_fmeta['step_cfg']
        input_filename = base_fmeta.input_filename

        logger.info(f'starting extract_regions')

        for region_name, region in step_cfg.region_crop.items():
            new_fmeta = AD(base_fmeta)
            new_fmeta.step_name = region_name
            new_fmeta.step_function = 'extract_regions'

            fpath, fname = self.create_filename(new_fmeta, ftype='region')
            new_fname = fpath + fname
            new_fmeta = self.generate_file_metadata(new_fname, new_fmeta.now, oldfmeta=base_fmeta)
            filename = new_fmeta.input_filename
            fpath, fname = filename.rsplit('/', 1)

            if not os.path.exists(fpath):
                shcmd(f'mkdir -p {fpath}')

            x, y, w, h = region[0:4]

            logger.info(f'extract_regions for {new_fmeta.nodename}: {region_name} - {region}')
            logger.info(f'extract_regions temp file: {filename}')

            if fmeta.ext in ['jpg']:
                # TODO; Ensure correct color space handling + depth handling
                img = cv2.imread(input_filename)
                img = img[y:y+h, x:x+w]
                cv2.imwrite(filename, img)
            elif fmeta.ext in ['mp4']:
                cmd = [
                    '/usr/bin/nice -n10 /usr/bin/ffmpeg -y -hide_banner',
                    f'-i {input_filename}',
                    f'-filter:v crop={w}:{h}:{x}:{y}',
                    '-c:a copy',
                    filename
                ]

                res = shcmd(' '.join(cmd))
                if res[-1] == 0:
                    logger.info(f'extract_regions succesfully cropped {new_fmeta.nodename} {region_name}')
                else:
                    logger.error(f'ERROR cropping video {res}')
                    del new_fmeta
                    continue
            else:
                logger.warning(f'Unexpected extension received in extract_regions: {fmeta.input_filename}')
                del new_fmeta
                continue

            fmeta_results.append(new_fmeta)
        return fmeta_results

    def extract_scene_clips(self, fmeta):
        try:
            clips = []
            if fmeta.block_clip_extraction:
                return clips
            for xm in fmeta.extraction_modes:
                for sidx, scene in enumerate(fmeta.scenes[xm]):
                    new_fmeta = AD(fmeta)
                    new_fmeta.extraction_modes = [xm]
                    new_fmeta.frames = scene
                    new_fmeta.scenes = [scene]
                    new_fmeta.total_frames = len(scene)
                    start_frame_num = scene[0][0]
                    stop_frame_num = scene[-1][0]
                    new_now = new_fmeta.now + timedelta(seconds=scene[0][1])
                    end_time = new_now + timedelta(seconds=scene[-1][1])
                    self.populate_time(new_fmeta, new_now, end_time=end_time)

                    fpath, fname = self.create_filename(fmeta, sid=sidx, scene=scene, ftype='clip')
                    filename = fpath + fname
                    if not os.path.exists(fpath):
                        shcmd(f'mkdir -p {cpath}')

                    cmd = [
                           '/usr/bin/ffmpeg',
                           f'-i {fmeta.input_filename}',
                           f'-vf select="between(n\,{start_frame_num}\,{stop_frame_num}),setpts=PTS-STARTPTS"',
                           filename
                    ]
                    res = shcmd(' '.join(cmd))
                    if res[-1] == 0:
                        clips.append(self.generate_file_metadata(filename, new_fmeta.now, oldfmeta=new_fmeta))
                    else:
                        logger.error(f'ERROR: scene clip extraction error output {res}')
        except Exception as err:
            logger.exception(f"File_Worker.extract_scene_clips exception occurred handling: {fmeta.input_filename}- {repr(err)}")
        return clips

    def extract_scene_frames(self, fmeta):
        try:
            frames = []
            if fmeta.block_frame_extraction:
                return frames
            if 'scenes' not in fmeta:
                return []
            for xm in fmeta.extraction_modes:
                for sidx, scene in enumerate(fmeta.scenes[xm]):
                    scene_frames = []
                    ref_image = None
                    compare = False
                    selected = scene[::fmeta.fxinterval]
                    if (fmeta.max_frames > 0) and (len(selected) > fmeta.max_frames):
                        selected = selected[:fmeta.max_frames]
                    logger.info(f"mode - {xm} - selected length: {len(selected)}")
                    for fm in selected:
                        try:
                            # fm unpacks to -> fnum, frame_time, ftype, ydiff
                            fnum, frame_time, ftype, ydiff = fm
                            new_fmeta = AD(fmeta)
                            new_fmeta.extraction_modes = [xm]
                            new_now = new_fmeta.now + timedelta(seconds=frame_time)
                            self.populate_time(new_fmeta, new_now)
                            logger.info(f"Extracting frame with iso-time {new_fmeta.now.isoformat()}: new_timestamp -> " +
                                        f"{new_fmeta.now.timestamp()} (base_time + frame_time) = {fmeta.now.timestamp()} + {timedelta(seconds=(frame_time))}")

                            fpath, fname = self.create_filename(fmeta, fcount=len(new_fmeta.frames), fnum=fnum, sid=sidx, scene=scene, ftype='frame')
                            filename = fpath + fname
                            if not os.path.exists(fpath):
                                shcmd('mkdir -p {}'.format(fpath))

                            cmd = [
                                '/usr/bin/ffmpeg',
                                '-i',
                                fmeta.input_filename,
                                f'-vf "select=eq(n\,{fnum})"',
                               '-vframes 1',
                                filename
                            ]
                            res = shcmd(' '.join(cmd))
                            if res[-1] == 0:
                                fm = self.generate_file_metadata(filename, new_fmeta.now, oldfmeta=new_fmeta)
                                if 'lpc' in fmeta.step_name:
                                    check = self.is_not_gray_image(filename)
                                    if check:
                                        if not compare:
                                            ref_image = load_image(filename)
                                            compare = True
                                            scene_frames.append(fm)
                                        elif fmeta.max_frames > 3:
                                            diff = self.compare_images(ref_image, self.load_image(filename))
                                            if diff >= (350 - check):
                                                scene_frames.append(fm)
                                            else:
                                                logger.info(f"{fname} not sufficiently different: {diff}")
                                                os.remove(filename)
                                                del fm
                                        else:
                                            scene_frames.append(fm)
                                    else:
                                        logger.info(f"{filename} is grey")
                                        os.remove(filename)
                                        del fm
                                else:
                                    scene_frames.append(fm)
                            else:
                                logger.info('scene frame extraction error: {}'.format(res))
                                if os.path.exists(filename):
                                    os.remove(filename)
                        except Exception as err:
                            logger.exception(f"File_Worker.extract_scene_frames exception occurred handling scene # {sidx}: {fmeta.input_filename}- {repr(err)}")

                    if len(scene_frames):
                        frames.extend(scene_frames)
        except Exception as err:
            logger.exception(f"File_Worker.extract_scene_frames exception occurred handling: {fmeta.input_filename}- {repr(err)}")
        return frames

    def generate_file_metadata(self, newfile, now, oldfmeta=None):
        #
        ##################################### JPEG
        # short_normal_fields_jpg (9) [M][R]
        #   0       1         2          3          4          5           6        7          8     (9)
        # ['var', 'lib', 'atollogy', 'images', 'nodename', 'gateway_id', 'date', 'event_type', 'fname'] (9)
        # /var/lib/atollogy/images/<ftp_path set to nodename>/<hostname set to gateway_id>/2018-10-19/pic_001/00.44.29[R][0@0][0].jpg
        #
        # short_ext_fields_jpg (11) [M][R]
        #   0       1         2          3          4          5              6              7           8        9         10 (11)
        # ['var', 'lib', 'atollogy', 'images', 'nodename', 'step_name', 'step_function', 'gateway_id', 'date', 'event_type', 'fname'] (11)
        # ['var', 'lib', 'atollogy', 'images', 'db8cam15_lehighhanson', 'lpc', 'p2', '38af294f1327', '2019-10-18', 'pic_001', '08.54.30[R][0@0][0].jpg'] (11)
        #
        # normal_fields_jpg (12) [M][R]
        #   0       1         2          3          4           5          6          7          8       9        10       11     (12)
        # ['var', 'lib', 'atollogy', 'images', 'nodename', 'gateway_id', 'date', 'event_type', 'ftype', 'hour', 'minute', 'fname'] (12)
        # /var/lib/atollogy/images/<ftp_path set to nodename>/<hostname set to gateway_id>/2018-09-07/<event_type>/<ftype>/<hour>/<minute>/<second>[<activation_type>][<total_frames>@<seq_number>][0].<ext>
        #
        # ext_fields_jpg (14) [M][R]
        #   0      1        2           3         4            5             6              7           8       9          10       11       12        13    (14)
        #['var', 'lib', 'atollogy', 'images', 'nodename', 'step_name', 'step_function', 'gateway_id', 'date', 'event_type', 'ftype', 'hour', 'minute', 'fname']
        # /var/lib/atollogy/images/<ftp_path set to nodename/step_name/step_function>/<hostname set to gateway_id>/2018-09-07/001/jpg/14/31/16[M][0@0][0].jpg
        #
        # motion_fields_jpg (12) [R][M]
        #   0       1         2          3          4           5              6         7        8       9        10       11     (12)
        # ['var', 'lib', 'atollogy', 'images', 'nodename', 'gateway_id', 'event_type', 'src',  'date', 'hour', 'minute', 'fname'] (12)
        # /var/lib/atollogy/images/nodename/gateway_id/<event_type>/rtsp/2018-09-07/<hour>/<minute>/<fname>
        # [R] = <second>_<collection_interval>[R].<ext>
        # [M] = <second>_<fnum>_<fps>[M].<ext>
        #
        # ext_motion_fields_jpg (14) [R][M]
        #   0       1         2          3          4           5            6               7              8          9       10      11      12         13 (14)
        # ['var', 'lib', 'atollogy', 'images', 'nodename', 'gateway_id', 'step_name', 'step_function', 'event_type', 'src',  'date', 'hour', 'minute', 'fname'] (14)
        # /var/lib/atollogy/images/nodename/gateway_id/<step_name>/<step_function>/activation/rtsp/2018-09-07/<hour>/<minute>/<second>_<fps>[M].<ext>
        # [R] = <second>_<collection_interval>[R].<ext>
        # [M] = <second>_<fnum>_<fps>[M].<ext>
        #
        # Frame extraction format (11) [FX]
        #    0      1        2           3         4            5             6              7           8        9            10       11       12    (13)
        # ['var', 'lib', 'atollogy', 'images', 'nodename', 'step_name', 'step_function', 'gateway_id', 'date', 'event_type', 'hour', 'minute', 'fname'] (13)
        #  /var/lib/atollogy/images/db8cam21_lehighhanson_prd/src/p1/38af294f146a/2019-11-26/001/09/04/43.475499[FX][228@151][0].jpg
        # ['var', 'lib', 'atollogy', 'images', 'db8cam15_lehighhanson', 'lpc', 'p2', '38af294f1327', '2019-10-18', '001', '10', '34', '08.3456[FX][224@174][0].jpg'] (11)
        #
        ##################################### VIDEO - dav_, dav, mp4
        # short_normal_fields_video (9) [M][R]
        #   0       1         2          3          4          5           6        7          8     (9)
        # ['var', 'lib', 'atollogy', 'images', 'nodename', 'gateway_id', 'date', 'event_type', 'fname'] (9)
        # ['var', 'lib', 'atollogy', 'images', 'db8cam16_lehighhanson_prd', '38af294f123a', '2019-10-23', 'video_001', '17.38.31[M][0@0][0].dav_] - 9
        #
        # normal_fields_video (11) [M][R]
        #   0       1         2          3          4          5           6          7           8       9       10    (11)
        # ['var', 'lib', 'atollogy', 'images', 'nodename', 'gateway_id', 'date', 'event_type', 'ftype', 'hour', 'fname'](11)
        # /var/lib/atollogy/images/db8cam05_lehighhanson_prd/38af294f13e5/2019-01-31/001/dav/22/22.13.57-22.13.57[M][0@0][0].dav_
        #
        # short_ext_fields_video (11) [M][R]
        #   0       1         2          3          4          5              6              7           8        9         10    (11)
        # ['var', 'lib', 'atollogy', 'images', 'nodename','step_name', 'step_function', 'gateway_id', 'date', 'event_type', 'fname'] (11)
        # ['var', 'lib', 'atollogy', 'images', 'db8cam15_lehighhanson', 'lpc', 'p2', '38af294f1327', '2019-10-18', 'video_001', '10.15.30-10.15.30[M][0@0][0].dav_'] (11)
        #
        # ext_fields_video (13) [M][R]
        #    0      1        2           3         4          5             6               7           8        9         10      11       12     (13)
        # ['var', 'lib', 'atollogy', 'images', 'nodename','step_name', 'step_function', 'gateway_id', 'date', 'event_type', 'ftype', 'hour', 'fname'] (13)
        #
        # motion_fields_video (12) [R][M]
        #   0       1         2          3          4           5              6         7        8       9        10       11     (12)
        # ['var', 'lib', 'atollogy', 'images', 'nodename', 'gateway_id', 'event_type', 'src',  'date', 'hour', 'minute', 'fname'] (12)
        # /var/lib/atollogy/images/nodename/gateway_id/activation/rtsp/2018-09-07/<hour>/<minute>/<second>_<fps>[M].<ext>
        #
        # ext_motion_fields_video (12) [R][M]
        #   0       1         2          3          4           5            6               7              8          9       10      11      12         13 (14)
        # ['var', 'lib', 'atollogy', 'images', 'nodename', 'gateway_id', 'step_name', 'step_function', 'event_type', 'src',  'date', 'hour', 'minute', 'fname'] (14)
        # /var/lib/atollogy/images/nodename/gateway_id/<step_name>/<step_function>/activation/rtsp/2018-09-07/<hour>/<minute>/<second>_<fps>[M].<ext>

        # Clip extraction format (11) [CX]
        #    0      1        2           3         4          5             6               7           8        9         10     (11)
        # ['var', 'lib', 'atollogy', 'images', 'nodename','step_name', 'step_function', 'gateway_id', 'date', 'event_type', 'fname'] (11)
        # /var/lib/atollogy/images/<ftp_path set to nodename>/<step_name>/<step_function>/<hostname set to gateway_id>/<date>/<event_type>/<start_h.m.s>-<end_h.m.s>[CX][<total_frames>@<seq_number>][0].<ext>
        #
        ################################## ACTi formats
        # ACTi B-Class format (6)
        #    0      1        2           3       4        5      (6)
        # ['var', 'lib', 'atollogy', 'images', 'path', 'fname']
        # /var/lib/atollogy/images/b76acti01_cesmiiatlsc_prd_000f7c172f29_2012-01-01/atlftpd_ci60_180256_613_1.raw
        #            0         1       2       3            4
        #          host       cgr     env   gateway_id   date
        # path: b76acti01_cesmiiatlsc_prd_000f7c172f29_2020-11-12
        #
        #            0         1       2       3            4
        # fname <step_name>_<step_function>_<hhmmss>_<ms>_<imgnum>.ext
        #
        # ACTi E-Class format (8)
        #    0      1        2           3          4           5              6            7     (8)
        # ['var', 'lib', 'atollogy', 'images', 'nodename', 'step_name', 'step_function', 'fname']
        # /var/lib/atollogy/images/z84acti01/atlftpd/snapshot/000f7c16cf31_20201111120151_3.jpg
        # fname: <gateway_id>_<datetime_str>_<fnum>.<ext>
        #
        # ACTi Z-Class format (9)
        #    0      1        2           3          4           5              6              7            8       (9)
        # ['var', 'lib', 'atollogy', 'images', 'nodename', 'gateway_id', 'step_name', 'step_function', 'fname']
        # /var/lib/atollogy/images/z84acti01/atlftpd/snapshot/000f7c16cf31_20201111120151_3.jpg
        # fname: <gateway_id>_<datetime_str>_<fnum>.<ext>
        #
        fmeta = AD()
        logger.info(f"generate_file_metadata newfile name: {newfile}")
        try:
            fparts = fmeta.fparts = [p for p in newfile.split("/") if p and len(p)]
            fmeta.is_fmeta = True
            fmeta.now = now
            fmeta.is_scheduled = True if '[R]' in fparts[-1] else False
            logger.info(f'fparts: {fparts} - {len(fparts)}')
            fmeta.ext = fparts[-1].rsplit(".", 1)[-1]
            fmeta.base_path = self.BCM.homedir
            fmeta.block_frame_extraction = False
            fmeta.block_clip_extraction = False
            fmeta.camera_device = "video0"
            fmeta.camera_id = 0
            fmeta.collection_interval = 60
            fmeta.dav_codec = 'h264'
            fmeta.drop_source = False
            fmeta.edge_steps = []
            fmeta.edge_routing = AD()
            fmeta.event_type = 'default'
            fmeta.extraction_modes = ['forward']
            fmeta.files = [newfile]
            fmeta.fxinterval = 3
            fmeta.fps = 5
            fmeta.input_filename = newfile
            fmeta.max_frames = 0
            fmeta.origin = False
            fmeta.return_src = False
            fmeta.separate_heartbeat = False
            fmeta.scene_bridge = 0
            fmeta.step_name = None
            fmeta.step_function = 'default'
            fmeta.threshold = 0
            fmeta.threshold_decay = 0.10
            fmeta.transcode_frame_rate = 5
            fmeta.vc_mode = 'VC'
            fmeta.path = "api/{}/image_reading".format(self.BCM.endpoints.atlapi.api_version)
            fmeta.uri = f"https://{self.BCM.endpoints.atlapi.pool}/{fmeta.path}"

            fpl = len(fparts)
            if fmeta.ext == 'jpg':
                fmeta.ftype = "image-{}".format(fmeta.ext)
                if 'acti' in fparts[4]:
                    if fpl == 6:  # B-Class Cameras
                        fmeta.update(dict(zip(self.acti_b_class_fields[4:], fparts[4:])))
                        fp_parts = fmeta.fpath.split('_')
                        fmeta.date = fp_parts[-1]
                        fmeta.gateway_id = fp_parts[-2]
                        nodename = '_'.join(fp_parts[:-2])
                        fmeta.step_name, fmeta.step_function, hhmmss, ms, fnum = fparts[-1].split('.')[0].split('_')
                        fmeta.fnum = int(fnum)
                        date_str = f'{fmeta.date}T{hhmmss[0:2]}:{hhmmss[2:4]}:{hhmmss[4:6]}.{ms}+0000'
                        self.populate_time(fmeta, date_str)
                    elif fpl == 8:  # E-Class Cameras
                        nodename = fparts[4]
                        fmeta.update(dict(zip(self.acti_e_class_fields[4:], fparts[4:])))
                        fmeta.gateway_id, datetime_str, fnum = fparts[-1].split('.')[0].split('_')
                        fmeta.fnum = int(fnum)
                        self.populate_time(fmeta, datetime_str)
                    elif fpl == 9:  # Z-Class Cameras
                        nodename = fparts[4]
                        fmeta.update(dict(zip(self.acti_z_class_fields[4:], fparts[4:])))
                        fmeta.fnum = int(fparts[-1].split('.')[0].split('_')[-1])
                        self.populate_time(fmeta, fparts[-1].split('.')[0].split('_')[1])
                    else:
                        logger.error(f"File_Worker.generate_file_metadata acti parse error: {newfile} - {fmeta}")
                        return None
                    if fmeta.event_type.isnumeric() and int(fmeta.event_type) in File_Worker.event_types:
                        fmeta.event_type = File_Worker.event_types[int(fmeta.event_type)]
                else:
                    nodename = fparts[4]
                    if '[FX]' in fparts[-1] and fpl == 13:
                        fmeta.update(dict(zip(self.frame_extraction_fields[4:], fparts[4:])))
                        fmeta.second, fmeta.microsecond = fparts[-1].split('[')[0].split('.')
                        fmeta.fnum = fparts[-1].rsplit('@', 1)[-1].split(']')[0]
                        fmeta.event_type = f"{fmeta.event_type}_fn{fmeta.fnum}"
                    elif (fpl > 7 and fparts[7] in self.motion_sources) or (fpl > 9 and fparts[9] in self.motion_sources):
                        if fpl == 14:
                            fmeta.update(dict(zip(self.ext_motion_fields_jpg[4:], fparts[4:])))
                        else:
                            fmeta.update(dict(zip(self.motion_fields_jpg[4:], fparts[4:])))
                        if fparts[-1].count('_'):
                            fmeta.second, fmeta.collection_interval = [int(x) for x in fparts[-1].split('[')[0].split('_')][:2]
                        else:
                            fmeta.second = int(fparts[-1].split('[')[0])
                    elif fpl == 9:
                        fmeta.update(dict(zip(self.short_normal_fields_jpg[4:], fparts[4:])))
                        fmeta.hour, fmeta.minute, fmeta.second = [int(v) for v in fparts[-1].split('[', 1)[0].split('.')]
                    elif fpl == 11:
                        fmeta.update(dict(zip(self.short_ext_fields_jpg, fparts)))
                        fmeta.hour, fmeta.minute, fmeta.second = [int(v) for v in fparts[-1].split('[')[0].split('.')]
                    elif fpl == 12:
                        fmeta.update(dict(zip(self.normal_fields_jpg[4:], fparts[4:])))
                        fmeta.second = fparts[-1][0:2]
                    elif fpl == 14:
                        fmeta.update(dict(zip(self.ext_fields_jpg, fparts)))
                        fmeta.second = fparts[-1][0:2]
                    else:
                        return None
            elif fmeta.ext in ['dav_', 'dav', 'mp4', 'raw']:
                fmeta.step_name = 'videoCapture'
                fmeta.step_function = 'videoRecording'
                fmeta.ftype = "video-{}".format(fmeta.ext)
                newfile_base = newfile.rsplit('.', 1)[0]
                fmeta.files.extend([f"{newfile_base}.{ext}" for ext in ['idx_', 'idx', 'tmp', 'dav', 'raw']])
                if 'acti' in fparts[4]:
                    if fpl == 6:  # B-Class Cameras
                        fmeta.update(dict(zip(self.acti_b_class_fields[4:], fparts[4:])))
                        fp_parts = fmeta.fpath.split('_')
                        fmeta.date = fp_parts[-1]
                        fmeta.gateway_id = fp_parts[-2]
                        nodename = '_'.join(fp_parts[:-2])
                        fmeta.step_name, fmeta.step_function, hhmmss, ms, fnum = fparts[-1].split('.')[0].split('_')
                        fmeta.fnum = int(fnum)
                        date_str = f'{fmeta.date}T{hhmmss[0:2]}:{hhmmss[2:4]}:{hhmmss[4:6]}.{ms}+0000'
                        self.populate_time(fmeta, date_str)
                    elif fpl == 8:  # E-Class Cameras
                        nodename = fparts[4]
                        fmeta.update(dict(zip(self.acti_e_class_fields[4:], fparts[4:])))
                        fmeta.gateway_id, datetime_str, fnum = fparts[-1].split('.')[0].split('_')
                        fmeta.fnum = int(fnum)
                        self.populate_time(fmeta, datetime_str)
                    elif fpl == 9:  # Z-Class Cameras
                        nodename = fparts[4]
                        fmeta.update(dict(zip(self.acti_z_class_fields[4:], fparts[4:])))
                        fmeta.fnum = int(fparts[-1].split('.')[0].split('_')[-1])
                        self.populate_time(fmeta, fparts[-1].split('.')[0].split('_')[1])
                    else:
                        logger.error(f"File_Worker.generate_file_metadata acti parse error: {newfile} - {fmeta}")
                        return None
                elif (fpl > 7 and fparts[7] in self.motion_sources) or (fpl > 9 and fparts[9] in self.motion_sources):
                    nodename = fparts[4]
                    if fpl == 14:
                        fmeta.update(dict(zip(self.ext_motion_fields_video[4:], fparts[4:])))
                    else:
                        fmeta.update(dict(zip(self.motion_fields_video[4:], fparts[4:])))
                    if fparts[-1].count('_'):
                        fmeta.second, fmeta.collection_interval = [int(x) for x in fparts[-1].split('[')[0].split('_')][:2]
                    else:
                        fmeta.second = int(fparts[-1].split('[')[0])
                elif (fpl > 7 and fparts[7] in self.frigate_sources) or (fpl > 9 and fparts[9] in self.frigate_sources):
                    nodename = fparts[4]
                    if fpl == 14:
                        fmeta.update(dict(zip(self.ext_frigate_fields_video[4:], fparts[4:])))
                    else:
                        fmeta.update(dict(zip(self.frigate_fields_video[4:], fparts[4:])))
                    fmeta.second, fmeta.fps = [int(x) for x in fparts[-1].split('[')[0].split('_')]
                else:
                    nodename = fparts[4]
                    fmeta.video_start = fparts[-1].split('[')[0]
                    if '-' in fmeta.video_start:
                        fmeta.video_start = fmeta.video_start.split('-')[0]
                    fmeta.hour, fmeta.minute, fmeta.second = [int(v) for v in fmeta.video_start.split('.')]
                    if fpl == 9:
                        fmeta.update(dict(zip(self.short_normal_fields_video[4:], fparts[4:])))
                    elif fpl == 11 and '[CX]' in fparts[-1]:
                        fmeta.update(dict(zip(self.clip_extraction_fields[4:], fparts[4:])))
                        clip_num = fparts[-1].rsplit('[', 1)[-1].split(']', 1)[0]
                        fmeta.event_type = f"{fmeta.event_type}_cn{clip_num}"
                    elif fpl == 11 and 'video_001' in fparts[9]:
                        fmeta.update(dict(zip(self.short_ext_fields_video[4:], fparts[4:])))
                    elif fpl == 11:
                        fmeta.update(dict(zip(self.normal_fields_video[4:], fparts[4:])))
                    elif fpl == 13:
                        fmeta.update(dict(zip(self.ext_fields_video[4:], fparts[4:])))
                    else:
                        return None
            else:
                return None

            fmeta.year, fmeta.month, fmeta.day = [int(p) for p in fmeta.date.split("-")]
            fmeta.fqdnd = nodename = parse_fqdn(self.parse_nodename(nodename))
            fmeta.update(fmeta.fqdnd)
            self.expand_step_parameters(fmeta)
            if oldfmeta is not None:
                for i in ['frames', 'total_frames', 'scenes', 'vcp']:
                    if i in oldfmeta:
                        fmeta[i] = oldfmeta[i]

            self.populate_time(fmeta, self.construct_time(fmeta))

            if f'_customer_cfg.devices.gateways.{fmeta.nodename}.svcs.{fmeta.scode}' in self.BCM:
                fmeta.update(self.BCM[f'_customer_cfg.devices.gateways.{fmeta.nodename}.svcs.{fmeta.scode}'])
                # logger.info(f'collection_interval found in customer_cfg: {fmeta.collection_interval}')
                logger.info(f"customer_cfg: {self.BCM[f'_customer_cfg.devices.gateways.{fmeta.nodename}.svcs.{fmeta.scode}']}")

            fmeta.filename = f"{fmeta.event_type}.{fmeta.ext}"
            fmeta.motion_name = "%(base_path)s/%(filename)s" % (fmeta)

            if fmeta.ext in ['mp4', 'idx_', 'dav_', 'raw']:
                fmeta.canonical_path = (
                    "image_readings/%(nodename)s/%(camera_device)s/%(step_name)s_video/%(date_hour)s"
                    % (fmeta)
                )
                fmeta.canonical_filename = (
                    "%(gateway_id)s_%(camera_device)s_%(collection_time)s_%(step_name)s_video-%(filename)s"
                    % (fmeta)
                )
                fmeta.s3_path = (
                    "s3://atl-%(env)s-%(cgr)s-rawdata/%(canonical_path)s/%(canonical_filename)s"
                    % (fmeta)
                )
            else:
                fmeta.canonical_path = (
                    "image_readings/%(nodename)s/%(camera_device)s/%(step_name)s/%(date_hour)s"
                    % (fmeta)
                )
                fmeta.canonical_filename = (
                    "%(gateway_id)s_%(camera_device)s_%(collection_time)s_%(step_name)s-%(filename)s"
                    % (fmeta)
                )
                fmeta.s3_path = (
                    "s3://atl-%(env)s-%(cgr)s-rawdata/%(canonical_path)s/%(canonical_filename)s"
                    % (fmeta)
                )
            fmeta.bundle_name = "/var/lib/atollogy/atlftpd/backlog/{}-{}".format(
                fmeta.nodename, fmeta.now.timestamp()
            )
            fmeta.bundle_json = fmeta.bundle_name + ".json"
            fmeta.bundle = [
                fmeta.camera_id,
                fmeta.collection_time,
                fmeta.customer_id,
                fmeta.gateway_id,
                fmeta.nodename
            ]
            fmeta.files.extend([fmeta.motion_name, fmeta.bundle_name, fmeta.bundle_json])

            return fmeta
        except Exception as err:
            logger.exception(f"File_Worker.generate_file_metadata exception occurred handling: {newfile} - {fmeta}: {repr(err)}")
            return None

    def generate_mvedge_stepcfg(self, fmeta):
        fmeta.stepCfg = AD(
            {
                "bundle": tuple(fmeta.bundle),
                "brightness": AD(
                    {
                        "alert": 3,
                        "low_threshold": 5,
                        "high_threshold": 120,
                        "normalize": False,
                        "track": True,
                        "track_history_length": 5,
                    }
                ),
                "camera_id": 0,
                "collection_time": fmeta.collection_time,
                "collection_interval": None,
                "color_space": "rgb",
                "config_version": FUNCTION_VERSION,
                "customer_id": fmeta.customer_id,
                "end_time": None,
                "filelist": [fmeta.filename],
                "function": fmeta.step_function,
                "function_version": FUNCTION_VERSION,
                "gateway_id": fmeta.gateway_id,
                "history": AD({"brightness": [], "past_values": []}),
                "input_crop": None,
                "input_filename": fmeta.input_filename,
                "input_images": [fmeta.filename],
                "input_image_indexes": [0],
                "input_parameters": {},
                "input_step": fmeta.nodename,
                "nodename": fmeta.nodename,
                "output": {},
                "processor_role": "mvedge",
                "region_filter": None,
                "startTime": fmeta.startTime,
                "step_name": fmeta.step_name,
                "version": "v01",
            }
        )

        if fmeta.step_function in [
                "gdpr_tf", "gdpr_f", "gdpr_coco",
                "annotate_tf", "annotate_f", "annotate_coco",
                "anonymize_tf", "anonymize_f",  "anonymize_coco"]:
            fmeta.stepCfg.subjects = AD(
                {
                    "personel": {
                        "color_space": "rgb",
                        "confidence_threshold": 0.6,
                        "nms_threshold": 0.4,
                        "states": {"person": [["bbox", None],
                            True,
                            'annotate' in fmeta.step_function,
                            True]},
                        "zone": None,
                    }
                }
            )

        if fmeta.stepCfg is not None:
            # logger.info("StepCfg: {}".format(stepCfg))
            fmeta.stepCfg.filedata = {
                fmeta.filename: base64.b64encode(
                    open(fmeta.input_filename, "rb").read()
                ).decode()
            }

    def is_not_gray_image(self, fname):
        if os.path.exists(fname):
            if 'mp4' in fname:
                return 130
            with open(fname, 'rb') as fh:
                image = cv2.imdecode(np.asarray(bytearray(fh.read()), dtype=np.uint8), cv2.IMREAD_COLOR)
                avg = np.mean(list(np.mean(image, axis=(0, 1)).astype(int)))
                del image
                logger.info(f"{fname} average is: {avg}")
                if int(avg) not in [127, 128, 129]:
                    return avg
                else:
                    return 0
        else:
            return 0

    def load_image(self, path):
        return cv2.imread(path)

    def make_bundle(self, btype, fmeta, path=None, headers=None, body=None):
        if btype not in ["api", "mqtt", "mvedge", "s3"]:
            return None
        try:
            bundle = AD(persistTGT=fmeta.bundle_name)
            bundle.bundle_name = fmeta.bundle_name
            bundle.btype = btype
            bundle.fmeta = fmeta
            bundle.headers = headers
            bundle.path = path
            bundle.body = body
            bundle.sync()
            return bundle
        except Exception as err:
            cleanup(fmeta)
            raise ATLFTPD_Error(
                "make bundle error for bundle {} with error: {}".format(
                    fmeta.bundle_name, err
                )
            )

    def parse_nodename(self, nodename):
        nparts = nodename.split('_')
        if len(nparts) == 3:
            host, cgr, env = nparts
        elif len(nparts) == 2:
            host, cgr = nparts
            env = self.BCM.env
        else:
            host = nparts[0]
            cgr = self.BCM.cgr
            env = self.BCM.env
        nodename = f'{host}_{cgr}_{env}'
        return nodename

    def populate_time(self, fmeta, now, end_time=None):
        if not isinstance(now, datetime):
            new_now = to_datetime(now)
        fmeta.now = fmeta.start_time = now
        fmeta.date =  str(now.date())
        fmeta.year, fmeta.month, fmeta.day = [int(p) for p in fmeta.date.split("-")]
        for t in ['year', 'month', 'day', 'hour', 'minute', 'second', 'microsecond']:
            fmeta[t] = int(getattr(fmeta.now, t))

        fmeta.timestamp = fmeta.collection_time = fmeta.now.timestamp()
        fmeta.startTime = fmeta.iso_datetime = now.isoformat()
        fmeta.date_hour = '/'.join([str(p) for p in [fmeta.year, fmeta.month, fmeta.day, fmeta.hour]])
        if end_time is not None:
            if not isinstance(end_time, datetime):
                end_time = to_datetime(end_time)
            fmeta.end_time = end_time
            fmeta.stopTime = fmeta.end_time.isoformat()
        else:
            fmeta.end_time = fmeta.start_time
            fmeta.stopTime = fmeta.end_time.isoformat()

    def process_edge_step(self, fmeta):
        '''returns an array of steps with processing applied if applicable'''
        step_results = []

        try:
            if fmeta.step_cfg.function == 'extract_regions':
                step_results.extend(self.extract_regions(fmeta))
            elif fmeta.step_cfg.function == 'mvedge':
                step_results.append(fmeta)
            else:
                logger.info(f'unable to process unknown edge step {fmeta.step_name}')
        except Exception as err:
            logger.error(f'ERROR: processing step: {err}')

        return step_results

    def process_file(self, newfile):
        try:
            now = datetime.utcnow()
            if newfile and os.path.exists(newfile):
                fmeta = self.generate_file_metadata(newfile, now)

                if not fmeta:
                    logger.error(f"ERROR:  parsing filename {newfile} failed - fmeta {fmeta} - removing file")
                    shcmd(f"rm -rf {newfile}")
                    return

                if fmeta and fmeta.ext in ['dav_', 'dav', 'raw']:
                    logger.info(f"starting mp4 conversion of {newfile}")
                    fmeta = self.convert_to_mp4(fmeta)
                    if fmeta is None:
                        logger.error(f"ERROR: convert_to_mp4 returned empty for - {newfile}")
                        shcmd(f"rm -rf {newfile}")
                        return

                steps = [fmeta.step_name]
                steps_to_process = AD({fmeta.step_name: fmeta})
                if f'_customer_cfg.devices.gateways.{fmeta.nodename}.edge_routing' in self.BCM:
                    edge_routing = self.BCM[f'_customer_cfg.devices.gateways.{fmeta.nodename}.edge_routing']
                    for step_name in [step_name for step_name in edge_routing.steps if step_name in edge_routing.step_cfgs]:
                        step_cfg = edge_routing.step_cfgs[step_name]
                        input_steps = [sn for sn in step_cfg.input_step.split('||') if sn in steps_to_process]
                        if len(input_steps):
                            new_fmeta = AD(steps_to_process[input_steps[0]])
                            new_fmeta.step_cfg = step_cfg
                            for step in self.process_edge_step(new_fmeta):
                                steps_to_process[step.step_name] = step
                                steps.append(step.step_name)

                for fmeta in [steps_to_process[step] for step in steps]:
                    send_events = []
                    se_len = 0
                    if fmeta.ext == 'jpg':
                        if 'fqdnd' in fmeta and fmeta.fqdnd.scode == 'atlbind':
                            logger.info(f"Starting document handling for: {newfile}")
                            self.undistort(fmeta.input_filename)
                            send_events.append(fmeta)
                        else:
                            send_events.append(fmeta)
                    else:
                        if fmeta.vc_mode in ['BX', 'FX', 'RX', 'SX', 'CX', 'DX', 'XX', 'XF', 'XR', 'ER']:
                            self.do_scene_detection(fmeta)
                            if fmeta.vc_mode[0:2] in ['BX', 'FX', 'RX', 'SX']:
                                logger.info(f"Starting frame extraction for: {newfile}")
                                send_events.extend(self.extract_scene_frames(fmeta))
                            elif fmeta.vc_mode[0:2] in ['CX']:
                                logger.info(f"Starting clip extraction for: {newfile}")
                                send_events.extend(self.extract_scene_clips(fmeta))
                            elif fmeta.vc_mode[0:2] in ['DX']:
                                logger.info(f"Starting document extraction for: {newfile}")
                                frames = self.extract_scene_frames(fmeta)
                                if len(frames):
                                    logger.info(f"DX frame count: {len(frames)}")
                                    self.undistort(frames[-1].input_filename)
                                    send_events.append(frames[-1])
                            elif fmeta.vc_mode[0:2] in ['XX', 'XF', 'XR']:
                                logger.info(f"Starting clip extraction for: {newfile}")
                                send_events.extend(self.extract_scene_clips(fmeta))
                                logger.info(f"Starting frame extraction from detected scenes")
                                send_events.extend([fm for fm in self.extract_scene_frames(fmeta) if  'is_fmeta' in fm])
                            if fmeta.return_src:
                                send_events.append(AD(fmeta))
                            else:
                                cleanup(fmeta)
                        elif not fmeta.return_src:
                            logger.info("Intentionally blocking source return!")
                        else:
                            send_events.append(AD(fmeta))
                            logger.info(f"No edge processing applied - forwarding {fmeta.filename} source to core")
                        logger.info(f"Added {len(send_events) - se_len} to send events")

                    logger.info("Total number of event payloads to be processed: {}".format(len(send_events)))
                    for fm in send_events:
                        if hasattr(fm, 'keys') and 'is_fmeta' in fm and fm.is_fmeta:
                            self.send_queue.put(fm.jstr())
                        else:
                            logger.error(f"ERROR: Non-fmeta entry in send events - removing {repr(fm)}")
                        del fm
                    del fmeta
        except Exception as err:
            logger.exception(f"File_Worker.process_files exception occurred handling: {newfile} - {repr(err)}")
            shcmd(f"rm -rf {newfile}")

    def reverse_video(self, fname):
        logger.info(f'reversing video file: {fname}')
        shcmd(f'mv {fname} {fname.rsplit(".", 1)[0]}_tmp.mp4')
        cmd = ['/usr/bin/ffmpeg',
               f'-i {fname.rsplit(".", 1)[0]}_tmp.mp4'
               f'-vf reverse',
               f'{fname}'
        ]
        res = shcmd(' '.join(cmd))
        shcmd(f'rm -rf {fname.rsplit(".", 1)[0]}_tmp.mp4')

    def subprocess_is_running(self, process):
        try:
            return process.returncode is None and process.poll() is None
        except:
            return 0

    def undistort(self, img_path, focal_length=2.8):
        img = cv2.imread(img_path)
        y, x = img.shape[:2]
        distCoeff = np.zeros((4,1),np.float64)
        k1 = -(focal_length/197500) # negative to remove barrel distortion
        # k1 = -1.0e-5
        k2 = 0
        p1 = 0.0
        p2 = 0.0

        distCoeff[0,0] = k1;
        distCoeff[1,0] = k2;
        distCoeff[2,0] = p1;
        distCoeff[3,0] = p2;

        # assume unit matrix for camera
        cam = np.eye(3,dtype=np.float32)
        cam[0,2] = x/2.0  # define center x
        cam[1,2] = y/2.0 # define center y

        if focal_length < 3.2:
            cam[0,0] = focal_length/0.45 # define focal length x
            cam[1,1] = focal_length/0.45 # define focal length y
        else:
            cam[0,0] = (focal_length/0.4) # define focal length x
            cam[1,1] = (focal_length/0.4) # define focal length y

        cv2.undistort(img, cam, distCoeff)
        cv2.imwrite(img_path, img)
        logger.info(f"File_Worker.undistort successfully undistorted: {img_path}")

class File_Handler():
    def __init__(self, bcm, work_queue, send_queue, late_queue):
        self.BCM = bcm
        self.work_queue = work_queue
        self.send_queue = send_queue
        self.late_queue = late_queue
        self.forwarding_mode = "direct"
        self.forwarding_role = "core"
        self.timeout = 65536
        self.check_time = 0
        self.avail = 0

    def run(self, *args, **kwargs):
        logger.info('Starting File_Handler Process')
        while True:
            try:
                if not self.work_queue.empty():
                    fn = self.work_queue.get(False)
                    if fn:
                        logger.info(f"File_Handler.run_loop processing work queue file: {fn}")
                        File_Worker(self.BCM, self.send_queue).process_file(fn)
                if not self.late_queue.empty():
                    fn = self.late_queue.get(False)
                    if fn:
                        File_Worker(self.BCM, self.send_queue).process_file(fn)
                        logger.info(f"File_Handler.run_loop processing late_queue file: {fn}")
                else:
                    time.sleep(0.1)
            except queue.Empty:
                time.sleep(0.25)
                pass
            except Exception as err:
                logger.exception(f"File_Handler.run_loop error: {repr(err)}")
                time.sleep(0.1)


class Multi_Handler(FTPHandler):

    def queue_add(self, newfile):
        fparts = newfile.split('/')
        self.work_queue.put(newfile)
        logger.info(f"Multi_Handler.queue_add: file {newfile} to work queue at depth: {self.work_queue.qsize()}")
        dd_metric(self.BCM, fparts[5], RECEIVE_METRIC, 1)

    def on_file_received(self, newfile):
        try:
            if ("ftp_test" in newfile or
                "testftp" in newfile or
                "DVRWorkDirectory" in newfile):
                logger.info(f"Bypassing the processing of ftp_test file: {newfile}")
                return True
            elif 'reboot_now' in newfile:
                shcmd('init 6')
            elif newfile[0] == '@':
                with open(newfile) as nf:
                    for cmd in nf.readlines():
                        shcmd(cmd)
                shcmd(f'mv {newfile} _{newfile[1:]}')
            elif 'idx' in newfile[-5:]:
                logger.info(f"Not processing file: {newfile}")
                return True
            elif newfile.rsplit('.', 1)[-1] in ['jpg', 'mp4', 'dav_', 'dav', 'raw']:
                self.queue_add(newfile)
            else:
                logger.info("Uncertain process result - not processing: {}".format(newfile))
                return
        except Exception as err:
            logger.exception(f"Multi_Handler.on_file_received exception: {newfile} - {repr(err)}")

    def on_incomplete_file_received(self, badfile):
        os.remove(badfile)
