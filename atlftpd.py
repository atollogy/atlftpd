#!/usr/bin/env python3.6

import multiprocessing as _mp
_mp.set_start_method('fork')

from attribute_dict import *
from common import *

import argparse
import concurrent.futures
from multi_handler import *
import os
from pyftpdlib.authorizers import DummyAuthorizer
from pyftpdlib.servers import ThreadedFTPServer
import queue
import signal
import sys
import threading
import time

import urllib3
urllib3.util.ssl_.DEFAULT_CIPHERS = 'ALL'
import requests
import requests.packages.urllib3.util.ssl_
requests.packages.urllib3.util.ssl_.DEFAULT_CIPHERS = 'ALL'

cpu_count = _mp.cpu_count() - 1
FILE_HANDLERS = cpu_count if cpu_count > 2 else 2

work_queue = _mp.Queue()
send_queue = _mp.Queue()
late_queue = _mp.Queue()

def get_cfg():
    try:
        parser = argparse.ArgumentParser(description="atollogy ftp server")
        parser.add_argument(
            "-c", "--config", default=None, help="override config file location"
        )
        parser.add_argument("-n", "--nat_addr", default=None, help="public ip being nat'ed")
        parser.add_argument("--no_mosquitto", default=False, action='store_true', help="public ip being nat'ed")
        parser.add_argument("-p", "--port", default=None, help="port")
        parser.add_argument(
            "-P",
            "--pasv_ports",
            default='49152-50192',
            help="pasv port range e.g '49152-50192'"
        )
        parser.add_argument(
            "-l", "--log_level", default=10, help="set logging level (ex: 10, 20)"
        )
        parser.add_argument("-H", "--HELP", help="Display extended help documentation")
        options = parser.parse_args()

        if options.log_level is not None:
            start_logger(int(options.log_level))
        elif "log_level" in BCM.svcs[BCM.scode]:
            start_logger(int(BCM.svcs[BCM.scode].log_level))
        else:
            start_logger(20)

        if options.port:
            BCM.svcs[BCM.scode].port = int(options.port)
        elif "port" not in BCM.svcs[BCM.scode]:
            BCM.svcs[BCM.scode].port = 21

        if "data_dir" not in BCM.svcs[BCM.scode]:
            BCM.svcs[BCM.scode].data_dir = "/var/lib/atollogy"

        BCM.homedir = BCM.svcs[BCM.scode].data_dir + "/images"

        for p in ['atlftpd/enqueued', 'atlftpd/backlog', 'atlftpd/incoming', 'images']:
            path = f"{BCM.svcs[BCM.scode].data_dir}/{p}"
            if not os.path.exists(path):
                os.makedirs(path)
            key =f"{p.split('/')[-1]}_dir"
            if not key in BCM.svcs[BCM.scode]:
                BCM.svcs[BCM.scode][key] = path
        return options

    except Exception as err:
        print("BCM load error: {}".format(err))
        sys.exit(1)

def get_ftpserver(options):
    authorizer = DummyAuthorizer()
    authorizer.add_user("db8cam", "db8cam,,,,", BCM.homedir, perm="elradfmwMT")
    authorizer.add_user("acti", "LetMeInPlease", BCM.homedir, perm="elradfmwMT")
    authorizer.add_user("operator", "LetMeSavePlease.1", BCM.homedir, perm="elradfmwMT")

    Multi_Handler.authorizer = authorizer
    Multi_Handler.BCM = BCM
    Multi_Handler.work_queue = work_queue

    if options.nat_addr:
        Multi_Handler.masquerade_address = options.nat_addr

    if options.pasv_ports:
        low, high = options.pasv_ports.split("-")
        Multi_Handler.passive_ports = range(int(low.strip()), int(high.strip()))

    ftp_server = ThreadedFTPServer(("", BCM.svcs[BCM.scode].port), Multi_Handler)
    return ftp_server

def load_queues():
    nn_files = AD()
    backlog_files = [f for f in shcmd('''grep 'input_filename' /var/lib/atollogy/atlftpd/backlog/*.json|cut -d '"' -f 4''')[0]
                     if f not in ['.', '..', './', '/'] and len(f) > 5 ]

    dirs = [f for f in shcmd('''find /var/lib/atollogy/images -maxdepth 1 -type d  ! -name "*DVRWorkDirectory*"''')[0]
            if f not in ['.', '..', './', '/'] and len(f) > 5 ]

    img_files = AD()
    cnt = 0
    for d in dirs:
        fl = sorted([f for f in shcmd(f'''find {d} -type f ! -name "*DVRWorkDirectory*" ! -name "*_"''')[0]
                     if f not in ['.', '..', './', '/'] and len(f) > 5], reverse=True)
        fl_len = len(fl)
        if fl_len:
            img_files[d] = fl
            cnt += fl_len

    logger.info(f'Load Queues should load {cnt} cache files into late queue')
    dk = list(img_files.keys())
    while cnt > 0:
        for d in dk:
            if len(img_files[d]):
                try:
                    f = img_files[d].pop()
                    late_queue.put(f)
                    time.sleep(0.001)
                except Exception as err:
                    logger.info(f"Late Queue load exception: {err}")
                cnt -= 1
    logger.info(f"Late Queue Size: {late_queue.qsize()}")


########################################################################################################################
if __name__ == "__main__":
    options = get_cfg()
    load_queues()
    # logger.info(f"ATLFTPD FILE HANDLER COUNT: {FILE_HANDLERS}")

    processes = []
    for x in range(1, 5):
        processes.append(_mp.Process(
            target=File_Handler(BCM, work_queue, send_queue, late_queue).run,
            args=(), name=f"FH-{x}", daemon=False)
        )
        processes.append(_mp.Process(
            target=File_Forwarder(BCM, send_queue).run,
            args=(), name=f'file_forwarder_{x}', daemon=False)
        )
        logger.info(f"atlftpd startup - spawned file-handling pair number: {x}")

    if not options.no_mosquitto:
        processes.append(_mp.Process(
            target=fs_watcher, args=(BCM, work_queue), name=f"fs_watcher", daemon=False)
        )
        processes.append(_mp.Process(
            target=MQTT_Watcher(BCM, work_queue=work_queue, incoming_dir='/var/lib/frigate/clips', qname='frigate/+/events/end', handler='frigate_event').run,
            args=(), name=f"rtsp_mqtt_watcher", daemon=False)
        )
        processes.append(_mp.Process(
            target=MQTT_Watcher(BCM, incoming_dir='/var/lib/motion', qname='atollogy/rtsp', work_queue=work_queue).run,
            args=(), name=f"rtsp_mqtt_watcher", daemon=False)
        )
        processes.append(_mp.Process(
            target=MQTT_Watcher(BCM, incoming_dir='/var/lib/motion', qname='atollogy/local', work_queue=work_queue).run,
            args=(), name=f"local_mqtt_watcher", daemon=False)
        )

    processes.append(_mp.Process(
        target=back_log_resend,
        args=(), name=f"backlog_resend", daemon=False)
    )
    processes.append(_mp.Process(
        target=get_ftpserver(options).serve_forever,
        args=(), name=f"ftpserver", daemon=False)
    )

    started = False
    t = 0
    while True:
        if not started:
            running_processes = [p.start() for p in processes]
            started = True
            logger.info('atlftpd started')

        if t >= 60:
            logger.info(f"Work Queue Size: {work_queue.qsize()}")
            logger.info(f"Send Queue Size: {send_queue.qsize()}")
            logger.info(f"Late Queue Size: {late_queue.qsize()}")
            t = 0
        else:
            time.sleep(0.01)
            t += 0.01
