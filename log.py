#!/usr/bin/env python3

import logging
from logging.handlers import RotatingFileHandler
import multiprocessing_logging
import os
import sys

APP_LOGGER = "atlftpd"

logger = logging.getLogger("atlftpd")

logging.basicConfig(
    level=logging.INFO,
    format='PID %(process)5s %(name)18s: %(message)s',
    stream=sys.stderr,
)
multiprocessing_logging.install_mp_handler()

def start_logger(log_level):
    LOG_PATH = "/var/log/atollogy"
    LOG_NAME = "{}/atlftpd.log".format(LOG_PATH)
    LOG_SIZE = 20000000  # 20MB
    LOG_ROTATION_COUNT = 10
    LOG_LEVEL = int(log_level)

    # Create log folder
    if not os.path.exists(LOG_PATH):
        os.makedirs(LOG_PATH)

    formatter = logging.Formatter(
        '{"timestamp": "%(asctime)s", "thread": "%(threadName)s", "syslog.appname": "%(name)s", "level": "%(levelname)s", "message": "%(message)s"}'
    )
    floghandler = RotatingFileHandler(
        LOG_NAME, maxBytes=LOG_SIZE, backupCount=LOG_ROTATION_COUNT
    )
    floghandler.setLevel(LOG_LEVEL)
    floghandler.setFormatter(formatter)

    logger.addHandler(floghandler)
    logger.setLevel(LOG_LEVEL)
